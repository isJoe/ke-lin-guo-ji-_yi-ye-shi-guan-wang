<?php 
	
	function _todoUpload ( $remote_file = '' , $local_file = '' ) {
		$conn_id = ftp_connect(SYNC_FTP_HOST,"21","2");

		// login with username and password
		$login_result = ftp_login($conn_id, SYNC_FTP_ACCOUNT, SYNC_FTP_PASSWORD);
		
		ftp_pasv($conn_id, true);

		// upload a file
		$status = ftp_put($conn_id, $remote_file, $local_file, FTP_BINARY) ;
		
		// close the connection
		ftp_close($conn_id);

		return $status ;
	}


	function lazywebUploadImage ( $local_file = '' , $filename = '' ) {
		$remote_file = SYNC_PROJECT.'/image/'.$filename;

		return _todoUpload( $remote_file , $local_file );
	}

	function lazywebUploadThumbnailImage ( $local_file = '' , $filename = '' ) {
		$remote_file = SYNC_PROJECT.'/image/cache/'.$filename;

		return _todoUpload( $remote_file , $local_file );
	}
