<?php
	// 取得$_GET 上面的參數,可以跳過不想要的參數值
	function _filterURIparameter ( $pass_list = array() ) {
		$uri = array();
		foreach ( $_GET as $key => $value ){
			if ( count($pass_list) == 0 ) {
				$uri[$key] = $value ;
			} else {
				if ( !in_array($key , $pass_list ) ) {
					$uri[$key] = $value ;
				}
			}
		}
		return urldecode(http_build_query( $uri ) ) ;
	}

	// 取得$_GET 上面的參數,如果該參數不存在時,可以設定預設的回傳數值
	function _getUrlParameterValue ( $key = '' , $default_value = '' ) {
		$val = '';
		if ( isset($_GET[$key]) ) {
			$val = $_GET[$key] ;
		}
		return ($val != '') ? $val : $default_value ;
	}

	// 取得$_POST 上面的參數,如果該參數不存在時,可以設定預設的回傳數值
	function _getPostParameterValue ( $key = '' , $default_value = '' ) {
		$val = '';
		if ( isset($_POST[$key]) ) {
			$val = $_POST[$key] ;
		}
		return ($val != '') ? $val : $default_value ;
	}

	// 取得 $_GET 上面指定的參數值，沒指定到的就不取
	function _getAssignUrlParameterValue ( $assign_list = array() ) {
		$uri = '';
		foreach ( $_GET as $key => $value ){
			if ( in_array( $key , $assign_list ) ) {
				if ( $uri == '') {
					$uri = $key . '=' . $value ;
				} else {
					$uri .= '&' . $key . '=' . $value ;
				}
			}
		}
		return $uri ;
	}


	function _setErrorMessageObject ($error = array() , $key_name ) {
		$error_message = array();
		if (isset($error[$key_name])) {
			$error_message = $error[$key_name];
		} else {
			$error_message = '';
		}
		return $error_message ;
	}


?>