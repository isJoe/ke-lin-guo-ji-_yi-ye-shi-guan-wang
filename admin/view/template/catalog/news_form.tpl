<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="/lazyweb/css/datetimepicker/jquery.datetimepicker.min.css"/>
<script src="/lazyweb/js/datetimepicker/datepicker-zh-TW.js"></script>
<script src="/lazyweb/js/datetimepicker/jquery.datetimepicker.full.min.js"></script>
<style>
    .base_font * {
        font-size: 13px !important;
    }
</style>

<div id="content">
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /><?php echo $text_action_page_title; ?></h1>
            <div class="buttons">
                <a onclick="saveAndStillEdit()" class="button">保存後繼續編輯</a>
                <a onclick="$('#form').submit();" class="button">保存後回查詢頁</a>
                <a href="<?php echo $cancel; ?>" class="button">取消</a>
            </div>
        </div>
        <div class="content base_font">

            <script>
                function saveAndStillEdit() {
                    $('form').attr('action', $('form').attr('action') + '&stillEdit' ) ;
                    $('#form').submit();
                }
            </script>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" autocomplete="off">
                <table class="form">
                    <tr>
                        <td class="td_title">
                            <span class="required">*</span> 封面小圖<br />
                            <span class="tip_red">建議尺寸600px * 600px<br />且檔名不能有中文跟特殊字元</span>
                        </td>
                        <td class="left">
                            <div class="image">
                                <img style="width: 150px;" src="<?php echo $image_url ?>" alt="" id="image_thumb_url">
                                <input type="hidden" name="image" value="<?php echo $image ?>" id="image">
                                <br>
                                <a onclick="file_upload('image', 'image_thumb_url')">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image', 'image_thumb_url')">清除</a>
                            </div>
                            <span class="error"><?php echo $error_image; ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_title">
                            內文大圖<br />
                            <span class="tip_red">建議尺寸1140px * 480px<br />且檔名不能有中文跟特殊字元</span>
                        </td>
                        <td class="left">
                            <div class="image">
                                <img style="width: 150px;" src="<?php echo $image_url2 ?>" alt="" id="image_thumb_url2">
                                <input type="hidden" name="image2" value="<?php echo $image2 ?>" id="image2">
                                <br>
                                <a onclick="file_upload('image2', 'image_thumb_url2')">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image2', 'image_thumb_url2')">清除</a>
                            </div>
                            <span class="error"><?php echo $error_image; ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_title"><span class="required">*</span> 標題</td>
                        <td>
                            <input type="text" name="title" value="<?php echo $title; ?>" style="width:450px;"/>
                            <span class="error"><?php echo $error_title; ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_title">
                            <span class="required">*</span> 簡介
                        </td>
                        <td>
                            <textarea name="intro" id="intro" cols="60"><?php echo $intro?></textarea>
                            <span class="error"><?php echo $error_intro; ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_title">
                            <span class="required">*</span> 詳細內容
                        </td>
                        <td>
                            <textarea class="literal_length" id ="description" name="description" rows="6" ><?php echo $description ?></textarea>
                            <span class="error"><?php echo $error_description; ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td class="td_title">排序</td>
                        <td>
                            <input type="text" name="sort_order" value="<?php echo $sort_order ?>" />
                            <span class="error"><?php echo $error_sort_order ?></span>
                        </td>
                    </tr>                    

                    <tr>
                        <td class="td_title">
                            <span class="required">*</span> 狀態
                        </td>
                        <td>
                            <select name="status">
                                <option value="1" <?php if ( $status == '1' ) { echo 'selected'; }?> >上架</option>
                                <option value="0" <?php if ( $status == '0' ) { echo 'selected'; }?> >關閉</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<!-- [START] FTP 上傳模式 -->
<form id="upload_file" action="index.php?route=common/filemanager/upload_file&token=<?php echo $token ?>" enctype="multipart/form-data" method="post" target="upload_file_endpoint" style="display: none;">
    <input type="file" name="fileToUpload" id="fileToUpload" onchange="file_change()">
</form>
<iframe name="upload_file_endpoint" id="upload_file_endpoint" style="display: none;"></iframe>
<script type="text/javascript">
    is_file_loading = 'N';
    image_path_id = '';
    image_path_url = '';
    function file_upload( id , url ) {
        if (is_file_loading == 'Y') {
            alert('有其他檔案上傳中！請稍後');
        } else {
            image_path_id = id ;
            image_path_url = url ;
            $('#fileToUpload').click();
        }
    }
    function image_error ( object ) {
        $(object).attr('src','<?php echo $no_image ?>');
    }
    function file_cancel ( id , url ) {
        $('#'+id).val('');
        $('#'+url).attr('src','<?php echo $no_image ?>');
    }
    function file_change() {
        if (is_file_loading == 'N') {
            if ( $('#fileToUpload').val() != '' ) {
                is_file_loading = 'Y';
                $('#upload_file').submit();
            } else {
                // 沒選擇檔案
                image_path_id = '';
                image_path_url = '';
            }

        } else {
            alert('檔案上傳中！請稍後');
        }
    }

    $('#upload_file_endpoint').on("load", function () {
        var contents = $(this).contents().find("body").html();
        var obj = JSON.parse(contents);

        if ( obj != '' && typeof(obj) != 'undefined' ) {
            if ( typeof(obj['status']) != 'undefined' && obj['status'] == '1' ) {
                $('#'+image_path_id).val(obj['path']);
                $('#'+image_path_url).attr('src',obj['url']);
                alert('上傳成功');
            } else if ( typeof(obj['error']) != 'undefined' ) {
                alert(obj['error']);
            } else {
                alert('上傳失敗! 確認檔案大小是否過大，或者有過多的特殊符號');
            }
        }
        $('#fileToUpload').val('');

        is_file_loading = 'N';
    });
</script>
<!-- FTP 上傳模式 [END] -->

<!-- [START] FTP 圖檔管理模組 -->
<script type="text/javascript">
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '圖檔管理',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: '<?php echo $_image; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
                        dataType: 'text',
                        success: function(text) {
                            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
                        }
                    });
                }
            },
            bgiframe: false,
            width: 800,
            height: 400,
            resizable: false,
            modal: false
        });
    };
</script>
<!-- FTP 圖檔管理模組  [END] -->

<!-- [START] 文字編輯器 -->
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.config.contentsCss = ['view/javascript/ckeditor/contents.css?1119'];
</script>
<script>
    CKEDITOR.replace('description' , {
        allowedContent: true,
        customConfig: '/admin/view/javascript/ckeditor/config.js?t=0605' ,
        filebrowserImageUploadUrl: 'index.php?route=common/filemanager/upload_file&token=<?php echo $token ?>',
        filebrowserUploadMethod : 'form' ,
        height: 500
    });
</script>
<!-- 文字編輯器 [END] -->


<?php echo $footer; ?>