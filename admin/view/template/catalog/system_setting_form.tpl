<?php echo $header; ?>

<div id="content">

    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>

    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>

    <?php if ($error) { ?>
        <div class="warning"><?php echo $error; ?></div>
    <?php } ?>

    <div class="box">

        <div class="heading">
            <h1><img src="view/image/product.png" alt="" />系統設定管理</h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button">保存</a></div>
        </div>

        <div class="content">

            <div id="tabs" class="htabs">
                <a href="#tab-1">banner下方背景圖</a>
                <a href="#tab-2">自訂程式碼</a>
                <a href="#tab-3">區塊順序</a>
            </div>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div id="tab-1">
                    <table class="form">
                        <tr>
                            <td class="td_title">
                                電腦版<br />
                                <span class="tip_red">建議尺寸1920px * 任意高度<br />且檔名不能有中文跟特殊字元</span>
                            </td>
                            <td>
                                <div class="image">
                                <img style="width: 150px;" src="<?php echo $image_url ?>" alt="" id="image_thumb_url">
                                    <input type="hidden" name="image" value="<?php echo $image ?>" id="image">
                                    <br>
                                    <a onclick="file_upload('image', 'image_thumb_url')">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image', 'image_thumb_url')">清除</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_title">
                                手機版<br />
                                <span class="tip_red">建議尺寸1000px * 任意高度<br />且檔名不能有中文跟特殊字元</span>
                            </td>
                            <td>
                                <div class="image">
                                <img style="width: 150px;" src="<?php echo $image_mb_url ?>" alt="" id="image_mb_thumb_url">
                                    <input type="hidden" name="image_mb" value="<?php echo $image_mb ?>" id="image_mb">
                                    <br>
                                    <a onclick="file_upload('image_mb', 'image_mb_thumb_url')">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image_mb', 'image_mb_thumb_url')">清除</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_title">
                                啟用狀態
                            </td>
                            <td>
                                <select name="home_block_0_status">
                                    <option value="1" <?php if ( $home_block_0_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_0_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>
                            </td>
                        </tr>                        
                    </table>
                </div>
                <div id="tab-2">
                    <table class="form">
                        <tr>
                            <td class="td_title">
                                &lt;head&gt;之後
                            </td>
                            <td>
                                <textarea name="head_after" cols="80"><?php echo $head_after?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_title">
                                &lt;/head&gt;之前
                            </td>
                            <td>
                                <textarea name="head_before" cols="80"><?php echo $head_before?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_title">
                                &lt;body&gt;之前
                            </td>
                            <td>
                                <textarea name="body_before" cols="80"><?php echo $body_before?></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="tab-3">
                    <table class="form">
                        <tr>
                            <td class="td_title">
                                關於科林 顯示順序
                            </td>
                            <td>
                                <select name="home_block_1">
                                    <?php for ( $i = 1 ; $i <= 6 ; $i++ ) { ?>
                                    <option value="<?php echo $i ?>" <?php if ( $i == $home_block_1 ) { echo 'selected'; } ?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                                <select name="home_block_1_status">
                                    <option value="1" <?php if ( $home_block_1_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_1_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>
                              <?php if ( isset($error_home_block_1) ) { ?>
                                <span class="error"><?php echo $error_home_block_1 ?></span>
                                <?php } ?>                                                                
                            </td>
                        </tr>
                        <tr>
                            <td class="td_title">
                                聽力保健 顯示順序
                            </td>
                            <td>
                                <select name="home_block_2">
                                    <?php for ( $i = 1 ; $i <= 6 ; $i++ ) { ?>
                                    <option value="<?php echo $i ?>" <?php if ( $i == $home_block_2 ) { echo 'selected'; } ?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                                <select name="home_block_2_status">
                                    <option value="1" <?php if ( $home_block_2_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_2_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>                                
                                <?php if ( isset($error_home_block_2) ) { ?>
                                <span class="error"><?php echo $error_home_block_2 ?></span>
                                <?php } ?>                                
                            </td>
                        </tr>    
                        <tr>
                            <td class="td_title">
                                服務流程 顯示順序
                            </td>
                            <td>
                                <select name="home_block_3">
                                    <?php for ( $i = 1 ; $i <= 6 ; $i++ ) { ?>
                                    <option value="<?php echo $i ?>" <?php if ( $i == $home_block_3 ) { echo 'selected'; } ?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                                <select name="home_block_3_status">
                                    <option value="1" <?php if ( $home_block_3_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_3_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>                                
                                <?php if ( isset($error_home_block_3) ) { ?>
                                <span class="error"><?php echo $error_home_block_3 ?></span>
                                <?php } ?>                                
                            </td>
                        </tr>        
                        <tr>
                            <td class="td_title">
                                讚聲好友 顯示順序
                            </td>
                            <td>
                                <select name="home_block_4">
                                    <?php for ( $i = 1 ; $i <= 6 ; $i++ ) { ?>
                                    <option value="<?php echo $i ?>" <?php if ( $i == $home_block_4 ) { echo 'selected'; } ?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                                <select name="home_block_4_status">
                                    <option value="1" <?php if ( $home_block_4_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_4_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>                                
                                <?php if ( isset($error_home_block_4) ) { ?>
                                <span class="error"><?php echo $error_home_block_4 ?></span>
                                <?php } ?>                                
                            </td>
                        </tr>   
                        <tr>
                            <td class="td_title">
                                常見問題 顯示順序
                            </td>
                            <td>
                                <select name="home_block_5">
                                    <?php for ( $i = 1 ; $i <= 6 ; $i++ ) { ?>
                                    <option value="<?php echo $i ?>" <?php if ( $i == $home_block_5 ) { echo 'selected'; } ?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                                <select name="home_block_5_status">
                                    <option value="1" <?php if ( $home_block_5_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_5_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>                                
                                <?php if ( isset($error_home_block_5) ) { ?>
                                <span class="error"><?php echo $error_home_block_5 ?></span>
                                <?php } ?>                                
                            </td>
                        </tr> 
                        <tr>
                            <td class="td_title">
                                聯絡我們 顯示順序
                            </td>
                            <td>
                                <select name="home_block_6">
                                    <?php for ( $i = 1 ; $i <= 6 ; $i++ ) { ?>
                                    <option value="<?php echo $i ?>" <?php if ( $i == $home_block_6 ) { echo 'selected'; } ?> ><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                                <select name="home_block_6_status">
                                    <option value="1" <?php if ( $home_block_6_status == '1' ) { echo 'selected'; } ?> >啟用</option>
                                    <option value="0" <?php if ( $home_block_6_status == '0' ) { echo 'selected'; } ?> >關閉</option>
                                </select>                                
                                <?php if ( isset($error_home_block_6) ) { ?>
                                <span class="error"><?php echo $error_home_block_6 ?></span>
                                <?php } ?>                                
                            </td>
                        </tr>
                    </table>
                </div>                
            </form>

        </div>
    </div>
</div>

<!-- 頁籤 -->
<script type="text/javascript">
    $('#tabs a').tabs();
</script>

<!-- [START] FTP 上傳模式 -->
<form id="upload_file" action="index.php?route=common/filemanager/upload_file&token=<?php echo $token ?>" enctype="multipart/form-data" method="post" target="upload_file_endpoint" style="display: none;">
    <input type="file" name="fileToUpload" id="fileToUpload" onchange="file_change()">
</form>
<iframe name="upload_file_endpoint" id="upload_file_endpoint" style="display: none;"></iframe>
<script type="text/javascript">
    is_file_loading = 'N';
    image_path_id = '';
    image_path_url = '';
    function file_upload( id , url ) {
        if (is_file_loading == 'Y') {
            alert('有其他檔案上傳中！請稍後');
        } else {
            image_path_id = id ;
            image_path_url = url ;
            $('#fileToUpload').click();
        }
    }
    function image_error ( object ) {
        $(object).attr('src','<?php echo $no_image ?>');
    }
    function file_cancel ( id , url ) {
        $('#'+id).val('');
        $('#'+url).attr('src','<?php echo $no_image ?>');
    }
    function file_change() {
        if (is_file_loading == 'N') {
            if ( $('#fileToUpload').val() != '' ) {
                is_file_loading = 'Y';
                $('#upload_file').submit();
            } else {
                // 沒選擇檔案
                image_path_id = '';
                image_path_url = '';
            }

        } else {
            alert('檔案上傳中！請稍後');
        }
    }

    $('#upload_file_endpoint').on("load", function () {
        var contents = $(this).contents().find("body").html();
        var obj = JSON.parse(contents);

        if ( obj != '' && typeof(obj) != 'undefined' ) {
            if ( typeof(obj['status']) != 'undefined' && obj['status'] == '1' ) {
                $('#'+image_path_id).val(obj['path']);
                $('#'+image_path_url).attr('src',obj['url']);
                alert('上傳成功');
            } else if ( typeof(obj['error']) != 'undefined' ) {
                alert(obj['error']);
            } else {
                alert('上傳失敗! 確認檔案大小是否過大，或者有過多的特殊符號');
            }
        }
        $('#fileToUpload').val('');

        is_file_loading = 'N';
    });
</script>
<!-- FTP 上傳模式 [END] -->

<!-- [START] FTP 圖檔管理模組 -->
<script type="text/javascript">
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '圖檔管理',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: '<?php echo $_image; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
                        dataType: 'text',
                        success: function(text) {
                            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
                        }
                    });
                }
            },
            bgiframe: false,
            width: 800,
            height: 400,
            resizable: false,
            modal: false
        });
    };
</script>
<!-- FTP 圖檔管理模組  [END] -->
<?php echo $footer; ?>