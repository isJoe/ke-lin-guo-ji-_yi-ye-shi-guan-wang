<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="/lazyweb/css/datetimepicker/jquery.datetimepicker.min.css"/>
<script src="/lazyweb/js/datetimepicker/datepicker-zh-TW.js"></script>
<script src="/lazyweb/js/datetimepicker/jquery.datetimepicker.full.min.js"></script>
<style>
    .base_font * {
        font-size: 13px !important; }
</style>
<style>
    .is_button {
        height: 33px;
        border-radius: 4px;
    }
    .is_text {
        height: 33px;
        border-radius: 4px;
    }
</style>
<div id="content">
    <?php if ($error_warning) { ?>  <div class="warning"><?php echo $error_warning; ?></div>    <?php } ?>
    <?php if ($success) { ?>    <div class="success"><?php echo $success; ?></div>  <?php } ?>

    <div class="box">
        <div class="heading">
            <h1><img src="view/image/product.png" alt="" /><?php echo $text_action_page_title; ?></h1>
            <div class="buttons">
                <a href="<?php echo $insert; ?>" class="button">新增</a>
                <a onclick="$('form').submit();" class="button">刪除</a>
            </div>
        </div>
        <div class="content base_font">

            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="list">
                    <thead>
                        <tr>
                            <td class="center" style="width: 2%;">
                                <input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
                            </td>
                            <td class="left" style="width: 10%;">
                                圖片
                            </td>
                            <td class="left" style="width: 18%;">
                                標題
                            </td>
                            <td class="left" style="width: 30%;">
                                簡介
                            </td>
                            <td class="left" style=" width: 10%; ">
                                排序
                            </td>
                            <td class="left" style="width: 10%">
                                狀態
                            </td>
                            <td class="center" style="width: 10%;">
                                動作
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ( !empty($news_list) ) : ?>
                            <?php foreach ($news_list as $row) : ?>
                            <tr>
                                <td class="center">
                                    <input type="checkbox" name="selected[]" value="<?php echo $row['id']; ?>" />
                                </td>

                                <td class="left"><img style="width: 150px;" src="<?php echo $row['image_url']; ?>"></td>

                                <td class="left"><?php echo $row['title']; ?></td>

                                <td class="left"><?php echo $row['intro']?></td>

                                <td class="left"><?php echo $row['sort_order'] ?></td>

                                <td class="left"><?php echo $row['status']; ?></td>

                                <td class="center">
                                    <?php foreach ($row['action'] as $action) : ?>
                                        <a href="<?php echo $action['href']; ?>">[ <?php echo $action['text']; ?> ]</a>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else :?>
                            <tr><td class="center" colspan="7"><?php echo $text_no_results; ?></td></tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </form>
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>
</div>

<?php echo $footer; ?>