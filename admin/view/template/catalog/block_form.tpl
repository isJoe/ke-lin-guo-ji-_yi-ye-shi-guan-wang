<?php echo $header; ?>

<div id="content">

    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>

    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>

    <?php if ($error) { ?>
        <div class="warning"><?php echo $error; ?></div>
    <?php } ?>

    <div class="box">

        <div class="heading">
            <h1><img src="view/image/banner.png" alt="" /><?php echo $text_action_page_title?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button">保存</a></div>
        </div>

        <div class="content">
            <?php if ($error_block_id) { ?>
                <div class="warning"><?php echo $error_block_id?></div>
            <?php } ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="block_id" value="<?php echo $block_id?>">
                <table class="form">
                    <tr>
                        <td class="td_title">
                            <span class="required">*</span> 背景圖<br />
                            <span class="tip_red">建議尺寸1920px * 1080px<br />且檔名不能有中文跟特殊字元</span>
                        </td>
                        <td>
                            <div class="image">
                                <img style="width: 150px;" src="<?php echo $image_url ?>" alt="" id="image_thumb_url">
                                <input type="hidden" name="image" value="<?php echo $image ?>" id="image">
                                <br>
                                <a onclick="file_upload('image', 'image_thumb_url')">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image', 'image_thumb_url')">清除</a>
                            </div>
                            <?php if (isset($error_image)) {?><span class="error"><?php echo $error_image?></span><?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_title">
                            <span class="required">*</span> 人像圖<br />
                            <span class="tip_red">建議尺寸600px * 645px<br />且檔名不能有中文跟特殊字元</span>
                        </td>
                        <td>
                            <div class="image">
                                <img style="width: 150px;" src="<?php echo $image2_url ?>" alt="" id="image2_thumb_url">
                                <input type="hidden" name="image2" value="<?php echo $image2 ?>" id="image2">
                                <br>
                                <a onclick="file_upload('image2', 'image2_thumb_url')">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image2', 'image2_thumb_url')">清除</a>
                            </div>
                            <?php if (isset($error_image2)) {?><span class="error"><?php echo $error_image2?></span><?php }?>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_title">
                            連結列表
                        </td>
                        <td>
                            <table id="images" class="list" style="margin: 0;">
                                <thead>
                                    <tr>
                                        <td class="left" style="width: 20%">標題</td>
                                        <td class="left" style="width: 40%">簡介</td>
                                        <td class="left" style="width: 30%">連結</td>
                                        <td class="center" style="width: 10%">動作</td>
                                    </tr>
                                </thead>

                                <?php $block_row = 0; ?>
                                <?php foreach ($block_info as $row): ?>
                                    <tbody id="image-row<?php echo $block_row; ?>">
                                        <tr>
                                            <td class="left">
                                                <input type="text" name="block[<?php echo $block_row; ?>][title]" value="<?php echo $row['title'];?>" />
                                                <? if (isset($error_block[$block_row]['title'])) {?><span class="error"><?php echo $error_block[$block_row]['title']?></span><?php }?>
                                            </td>
                                            <td class="left">
                                                <input type="text" name="block[<?php echo $block_row; ?>][intro]" value="<?php echo $row['intro'];?>" />
                                                <? if (isset($error_block[$block_row]['title'])) {?><span class="error"><?php echo $error_block[$block_row]['intro']?></span><?php }?>
                                            </td>
                                            <td class="left">
                                                <input type="text" name="block[<?php echo $block_row; ?>][link]" value="<?php echo $row['link'];?>" />
                                            </td>
                                            <td class="center" >
                                                <a onclick="$('#image-row<?php echo $block_row; ?>').remove();" class="button">移除</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <?php $block_row++; ?>
                                <?php endforeach ?>

                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td class="center"><a onclick="addBlock();" class="button">新增</a></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                </table>

            </form>
        </div>

    </div>

</div>

</div>

<script type="text/javascript">

    var block_row = '<?php echo $block_row; ?>';

    function addBlock() {

        html  = '<tbody id="image-row' + block_row + '">';

        html += '<tr>';

        html += '<td class="left"><input type="text" name="block[' + block_row + '][title]" value="" /></td>';

        html += '<td class="left"><input type="text" name="block[' + block_row + '][intro]" value="" /></td>';

        html += '<td class="left"><input type="text" name="block[' + block_row + '][link]" value="" /></td>';

        html += '<td class="center"><a onclick="$(\'#image-row' + block_row  + '\').remove();" class="button">移除</a></td>';

        html += '</tr>';

        html += '</tbody>';

        $('#images tfoot').before(html);

        block_row++;
    }
</script>

<!-- [START] FTP 上傳模式 -->
<form id="upload_file" action="index.php?route=common/filemanager/upload_file&token=<?php echo $token ?>" enctype="multipart/form-data" method="post" target="upload_file_endpoint" style="display: none;">
    <input type="file" name="fileToUpload" id="fileToUpload" onchange="file_change()">
</form>
<iframe name="upload_file_endpoint" id="upload_file_endpoint" style="display: none;"></iframe>
<script type="text/javascript">
    is_file_loading = 'N';
    image_path_id = '';
    image_path_url = '';
    function file_upload( id , url ) {
        if (is_file_loading == 'Y') {
            alert('有其他檔案上傳中！請稍後');
        } else {
            image_path_id = id ;
            image_path_url = url ;
            $('#fileToUpload').click();
        }
    }
    function image_error ( object ) {
        $(object).attr('src','<?php echo $no_image ?>');
    }
    function file_cancel ( id , url ) {
        $('#'+id).val('');
        $('#'+url).attr('src','<?php echo $no_image ?>');
    }
    function file_change() {
        if (is_file_loading == 'N') {
            if ( $('#fileToUpload').val() != '' ) {
                is_file_loading = 'Y';
                $('#upload_file').submit();
            } else {
                // 沒選擇檔案
                image_path_id = '';
                image_path_url = '';
            }

        } else {
            alert('檔案上傳中！請稍後');
        }
    }

    $('#upload_file_endpoint').on("load", function () {
        var contents = $(this).contents().find("body").html();
        var obj = JSON.parse(contents);

        if ( obj != '' && typeof(obj) != 'undefined' ) {
            if ( typeof(obj['status']) != 'undefined' && obj['status'] == '1' ) {
                $('#'+image_path_id).val(obj['path']);
                $('#'+image_path_url).attr('src',obj['url']);
                alert('上傳成功');
            } else if ( typeof(obj['error']) != 'undefined' ) {
                alert(obj['error']);
            } else {
                alert('上傳失敗! 確認檔案大小是否過大，或者有過多的特殊符號');
            }
        }
        $('#fileToUpload').val('');

        is_file_loading = 'N';
    });
</script>
<!-- FTP 上傳模式 [END] -->

<!-- [START] FTP 圖檔管理模組 -->
<script type="text/javascript">
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '圖檔管理',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: '<?php echo $_image; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
                        dataType: 'text',
                        success: function(text) {
                            $('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
                        }
                    });
                }
            },
            bgiframe: false,
            width: 800,
            height: 400,
            resizable: false,
            modal: false
        });
    };
</script>
<!-- FTP 圖檔管理模組  [END] -->

<?php echo $footer; ?>