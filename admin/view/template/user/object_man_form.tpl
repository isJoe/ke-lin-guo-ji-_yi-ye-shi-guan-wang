<?php echo $header; ?>

<div class="base_font" id="content">
	
	<div class="box">	
		<div class="heading">
			<h1 style=" font-size: 1.4em !important; "><img src="view/image/setting.png" alt="" />會員修改(募資者) - 修改 >> <font color="blue" style=" font-size: 1.2em !important; ">守夜人</font>  </h1> 						
			<div class="buttons">
				<a onclick="$('#form').submit();" class="button">儲存</a>
				<a href="javascript:void(0)" class="button">取消</a>
			</div>
		</div>
		
		<div class="content">
			<form action="" method="post" enctype="multipart/form-data" id="form">	
			
				<div class="htabs">
					<a href="#tab-general" class="selected" style="display: inline;">個人資料</a>
					<a href="#tab-object" style="display: inline;">申請的募資案列表</a>
				</div>		
							
				<div id="tab-general" >
					<div class="vtabs">		
						<a href="#tab-customer" style="text-align:center; height: 30px;line-height: 30px;" class="selected">個人資料</a>				
						<a href="#tab-acount" style="text-align:center; height: 30px;line-height: 30px;" class="selected">登入密碼修改</a> <!-- 限定一般註冊才可以使用的功能 --> 
					</div>		
																								
					<!-- 個人資料 tab -->
					<div id="tab-customer" class="vtabs-content">
						<table class="form group">
							<thead>
								<tr>
									<td class="group_title" colspan="4"><b>基礎資訊</b></td>
								</tr>
							</thead>						
							<tbody>
								<tr>
									<td class="td_title">
										註冊時間 
									</td>		
									<td class="td_content">
										2017-03-08 12:23:31
									</td>
								</tr>
								<tr>
									<td class="td_title">
										最後登入時間 
									</td>		
									<td class="td_content">
										2017-03-25 12:23:31
									</td>
								</tr>									
							</tbody>
						</table>
					
						<table class="form group">
							<thead>
								<tr>
									<td class="group_title" colspan="4"><b>個人資料</b></td>
								</tr>
							</thead>						
							<tbody>
								<tr>
									<td class="td_title">
										登入帳戶
									</td>		
									<td class="td_content">
										joe20330@gmail.com
									</td>	


									<td class="td_title">
										註冊方式
									</td>		
									<td class="td_content">
										一般註冊/Facebook註冊/google註冊/linkedin註冊
									</td>	
																
								</tr>													
								<tr>
									<td class="td_title">
										<span class="required">*</span> 姓氏
									</td>
									<td class="td_content"> 
										<input type="text" name="firstname" value="">
									</td>
									<td class="td_title">
										<span class="required">*</span> 名字
									</td>
									<td class="td_content"> 
										<input type="text" name="lastname" value="">
									</td>						
								</tr>		
								<tr>
									<td class="td_title">
										<span class="required">*</span> 暱稱
									</td>
									<td class="td_content"> 
										<input type="text" name="name" value="">
									</td>
									<td></td>
									<td></td>					
								</tr>								
								<tr>
									<td class="td_title">
										聯絡電話
									</td>
									<td class="td_content"> 
										<input type="text" name="firstname" value="">
									</td>
									<td class="td_title">
										<span class="required">*</span> 帳戶狀態
									</td>
									<td class="td_content"> 
										<select name="status" >
											<option value="1">有效</option>
											<option value="0">停用</option>
										</select>
									</td>				
								</tr>		
							</tbody>
						</table>		
						<table class="form group">
							<thead>
								<tr>
									<td class="group_title" colspan="4"><b>後端人員備註訊息</b></td>
								</tr>
							</thead>
							
							<tbody>						
								<tr>
									<td colspan="4" class="group_title">
										<textarea name="remark" rows="2" placeholder="客戶的其他備註訊息可以填寫在此"></textarea>
									</td>
								</tr>													
							</tbody>
						</table>	
					</div>
					<!-- 密碼管理 tab -->
					<div id="tab-acount" class="vtabs-content" >
						<table class="form group">
							<thead>
								<tr>
									<td class="group_title" colspan="4"><b>修改密碼</b></td>
								</tr>
							</thead>						
							<tbody>
								<tr>
									<td class="td_title">
										輸入欲更改的密碼 
									</td>		
									<td class="td_content">
										<input type="password" name="password" value="">
									</td>
								</tr>
								<tr>
									<td class="td_title">
										再輸入一次密碼 
									</td>		
									<td class="td_content">
										<input type="password" name="password_confirm" value="">
									</td>
								</tr>									
							</tbody>
						</table>
					</div>						
				</div>			
			

				<div id="tab-object" >
					<table class="list">
						<thead>
							<tr>
								<td class="center"></td>
								<td class="left">募資案名稱</td>
								<td class="left">狀態</td>
								
								<td class="center">剩餘天數</td>
																
								<td class="center">募資總金額(萬)</td>
								<td class="center">目前募資金額(萬)</td>
								
								<td class="left">新增時間</td>
								<td class="left">申請時間</td>
								<td class="left">上架時間</td>								
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td class="center">
									[ <a target="_blank" href="javascript:void(0)">查看</a> ]
								</td>							
								<td class="left">李榮浩 - 不說 『從你的全世界路過 電影主題曲』</td>
								<td class="left">尚未申請</td>
								<td class="center"> - </td>
								<td class="center"> - </td>
								<td class="center"> - </td>
								<td class="left">2017-03-08 11:58:24</td>
								<td class="left"> - </td>
								<td class="left"> - </td>
							</tr>						
						
							<tr>
								<td class="center">
									[ <a target="_blank" href="javascript:void(0)">查看</a> ]
								</td>							
								<td class="left">【擺渡人】愛情版主題曲《讓我留在你身邊》/ 陳奕迅 演唱</td>
								<td class="left">申請中</td>
								<td class="center"> - </td>
								<td class="center"> 3 </td>
								<td class="center"> - </td>
								<td class="left">2017-03-08 11:58:24</td>
								<td class="left">2017-03-08 12:30:00</td>
								<td class="left"> - </td>
							</tr>
							
							<tr>
								<td class="center">
									[ <a target="_blank" href="javascript:void(0)">查看</a> ]
								</td>							
								<td class="left">金剛戰士主題曲 【Power Rangers】新舊版混搭（自製）</td>
								<td class="left">已公開</td>
								<td class="center"> 28 </td>
								<td class="center"> 200 </td>
								<td class="center"> 158 </td>
								<td class="left">2017-03-01 11:58:24</td>
								<td class="left">2017-03-02 12:30:00</td>
								<td class="left">2017-03-08 12:30:00</td>
							</tr>							
							
						</tbody>
					</table>			
								
				</div>
				
				
			</form>	
		</div>

	</div>
		
</div>

				
<?php echo $footer; ?> 

<script type="text/javascript">
	$('.htabs a').tabs();
	$('.vtabs a').tabs();
</script>
