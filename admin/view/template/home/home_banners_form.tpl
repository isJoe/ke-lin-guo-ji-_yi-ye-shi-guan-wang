<?php echo $header; ?>

<div id="content">


  <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>

  <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
  <?php } ?>

  <?php if ($error) { ?>
    <div class="warning"><?php echo $error; ?></div>
  <?php } ?>

    <div class="box">

        <div class="heading">

            <h1><img src="view/image/banner.png" alt="" />首頁輪播 管理</h1>

            <div class="buttons"><a onclick="$('#form').submit();" class="button">保存</a></div>

        </div>

        <div class="content">

          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

            <table id="images" class="list">

              <thead>
                <tr>
                  <td class="left" style="width: 8%"><span class="required">*</span> 圖片<br /><span class="tip_red">建議尺寸1920px * 850px<br />且檔名不能有中文跟特殊字元</span></td>
                  <td class="left" style="width: 8%"><span class="required">*</span> 手機圖片<br /><span class="tip_red">建議尺寸1000px * 800px<br />且檔名不能有中文跟特殊字元</span></td>
                  <td class="left" style="width: 19%">標題</td>
                  <td class="left" style="width: 19%">副標題</td>
                  <td class="left" style="width: 19%">按鈕連結</td>
                  <td class="left" style="width: 10%">按鈕文字</td>
                  <td class="left" style="width: 10%">排序<br /><span class="tip_red">由0開始排序<br>數字越小越前面</span></td>
                  <td class="left" style="width: 10%">狀態</td>
                  <td class="center" style="width: 5%">動作</td>
                </tr>
              </thead>

              <?php $image_row = 0; ?>

              <?php foreach ($banner_info as $row): ?>

                  <tbody id="image-row<?php echo $image_row; ?>">

                    <tr>
                        <td class="left">
                          <div class="image">
                            <img style="width: 150px;" src="<?php echo $row['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                            <input type="hidden" name="home_banners[<?php echo $image_row; ?>][image]" value="<?php echo $row['image']; ?>" id="image<?php echo $image_row; ?>"  />
                            <br />
                            <a onclick="file_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');">清除</a>
                          </div>

                            <?php if(!empty($error_home_banners[$image_row]['image'])): ?>
                                <span class="error"><?php echo $error_home_banners[$image_row]['image']; ?></span>
                            <?php endif;?>
                        </td>

                        <td class="left">

                          <div class="image">
                            <img style="width: 150px;" src="<?php echo $row['thumb_mobile']; ?>" alt="" id="thumb_mobile<?php echo $image_row; ?>" />
                            <input type="hidden" name="home_banners[<?php echo $image_row; ?>][image_mobile]" value="<?php echo $row['image_mobile']; ?>" id="image_mobile<?php echo $image_row; ?>"  />
                            <br />
                            <a onclick="file_upload('image_mobile<?php echo $image_row; ?>', 'thumb_mobile<?php echo $image_row; ?>');">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel('image_mobile<?php echo $image_row; ?>', 'thumb_mobile<?php echo $image_row; ?>');">清除</a>
                          </div>

                            <?php if(!empty($error_home_banners[$image_row]['image_mobile'])): ?>
                                <span class="error"><?php echo $error_home_banners[$image_row]['image_mobile']; ?></span>
                            <?php endif;?>
                        </td>

                        <td class="left">
                            <textarea name="home_banners[<?php echo $image_row; ?>][title]"><?php echo $row['title'];?></textarea>
                        </td>

                        <td class="left">
                            <textarea name="home_banners[<?php echo $image_row; ?>][sub_title]"><?php echo $row['sub_title'];?></textarea>
                        </td>

                        <td class="left">
                            <input type="text" name="home_banners[<?php echo $image_row; ?>][link]" value="<?php echo $row['link'];?>" />
                        </td>

                        <td class="left">
                            <input type="text" name="home_banners[<?php echo $image_row; ?>][link_text]" value="<?php echo $row['link_text'];?>" />
                        </td>

                        <td class="left"><input type="text" name="home_banners[<?php echo $image_row; ?>][sort_order]" value="<?php echo $row['sort_order']; ?>" / size="3"></td>

                        <td class="left">
                            <select name="home_banners[<?php echo $image_row; ?>][status]">
                                <option value="1" <?php echo ($row['status'] == '1')?'selected':'';?>>啟用</option>
                                <option value="0" <?php echo ($row['status'] == '0')?'selected':'';?>>停用</option>
                            </select>
                        </td>

                        <td class="center" ><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="button">移除</a></td>

                    </tr>

                  </tbody>

                    <?php $image_row++; ?>

              <?php endforeach ?>

              <tfoot>
                <tr>
                  <td colspan="8"></td>
                  <td class="left"><a onclick="addImage();" class="button">新增輪播</a></td>
                </tr>
              </tfoot>

            </table>
          </form>
        </div>

      </div>

    </div>

</div>

  <script type="text/javascript">

    var image_row = '<?php echo $image_row; ?>';

    function addImage() {

        html  = '<tbody id="image-row' + image_row + '">';

        html += '<tr>';

        html += '<td class="left"><div class="image"><img style="width: 150px;" src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="home_banners[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a style="width: 150px;" onclick="file_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel(\'image' + image_row + '\', \'thumb' + image_row + '\');">清除</a></div></td>';

        html += '<td class="left"><div class="image"><img style="width: 150px;" src="<?php echo $no_image; ?>" alt="" id="thumb_mobile' + image_row + '" /><input type="hidden" name="home_banners[' + image_row + '][image_mobile]" value="" id="image_mobile' + image_row + '" /><br /><a onclick="file_upload(\'image_mobile' + image_row + '\', \'thumb_mobile' + image_row + '\');">上傳檔案</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="file_cancel(\'image_mobile' + image_row + '\', \'thumb_mobile' + image_row + '\');">清除</a></div></td>';

        html += '<td class="left"><textarea type="text" name="home_banners[' + image_row + '][title]"></textarea></td>';

        html += '<td class="left"><textarea type="text" name="home_banners[' + image_row + '][sub_title]"></textarea></td>';

        html += '<td class="left"><input type="text" name="home_banners[' + image_row + '][link]" value="" /></td>';

        html += '<td class="left"><input type="text" name="home_banners[' + image_row + '][link_text]" value="" /></td>';

        html += '<td class="left"><input type="text" name="home_banners[' + image_row + '][sort_order]" value="' + image_row + '" size="3" /></td>';

        html += ' <td class="left"> <select name="home_banners[' + image_row + '][status]"> <option value="1" >啟用</option> <option value="0" >停用</option> </select> </td>';

        html += '<td class="center"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="button">移除</a></td>';

        html += '</tr>';

        html += '</tbody>';

        $('#images tfoot').before(html);

        image_row++;
    }

  </script>

<!-- [START] FTP 上傳模式 -->
    <form id="upload_file" action="index.php?route=common/filemanager/upload_file&token=<?php echo $token ?>" enctype="multipart/form-data" method="post" target="upload_file_endpoint" style="display: none;">
        <input type="file" name="fileToUpload" id="fileToUpload" onchange="file_change()">
    </form>
    <iframe name="upload_file_endpoint" id="upload_file_endpoint" style="display: none;"></iframe>
    <script type="text/javascript">
        is_file_loading = 'N';
        image_path_id = '';
        image_path_url = '';
        function file_upload( id , url ) {
            if (is_file_loading == 'Y') {
                alert('有其他檔案上傳中！請稍後');
            } else {
                image_path_id = id ;
                image_path_url = url ;
                $('#fileToUpload').click();
            }
        }
        function image_error ( object ) {
            $(object).attr('src','<?php echo $no_image ?>');
        }
        function file_cancel ( id , url ) {
            $('#'+id).val('');
            $('#'+url).attr('src','<?php echo $no_image ?>');
        }
        function file_change() {
            if (is_file_loading == 'N') {
                if ( $('#fileToUpload').val() != '' ) {
                    is_file_loading = 'Y';
                    $('#upload_file').submit();
                } else {
                    // 沒選擇檔案
                    image_path_id = '';
                    image_path_url = '';
                }

            } else {
                alert('檔案上傳中！請稍後');
            }
        }

        $('#upload_file_endpoint').on("load", function () {
            var contents = $(this).contents().find("body").html();
            var obj = JSON.parse(contents);

            if ( obj != '' && typeof(obj) != 'undefined' ) {
                if ( typeof(obj['status']) != 'undefined' && obj['status'] == '1' ) {
                    $('#'+image_path_id).val(obj['path']);
                    $('#'+image_path_url).attr('src',obj['url']);
                    alert('上傳成功');
                } else if ( typeof(obj['error']) != 'undefined' ) {
                    alert(obj['error']);
                } else {
                    alert('上傳失敗! 確認檔案大小是否過大，或者有過多的特殊符號');
                }
            }
            $('#fileToUpload').val('');

            is_file_loading = 'N';
        });
    </script>
<!-- FTP 上傳模式 [END] -->

<?php echo $footer; ?>