<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
	<meta charset="UTF-8" />
	<title>後台 - <?php echo $title; ?> :: 科林助聽器</title>
	<link rel="shortcut icon" href="/lazyweb/web/images/favicon_g.png" />
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>
	<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css?20170301" />
	<link rel="stylesheet" type="text/css" href="view/stylesheet/joe_style.css?20170301-2" />
	<?php foreach ($styles as $style) { ?>
	<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/e_style.css?20170301" rel="stylesheet" />
	<script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>
	<script type="text/javascript" src="view/javascript/common.js?121201"></script>
	<?php foreach ($scripts as $script) { ?>
	<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php } ?>
	<script type="text/javascript">

	//-----------------------------------------
	// Confirm Actions (delete, uninstall)
	//-----------------------------------------

	$(document).ready(function(){
		// Confirm Delete
		$('#form').submit(function(){
			if ($(this).attr('action').indexOf('delete',1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});
		// Confirm Uninstall
		$('a').click(function(){
			if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});
	});
	</script>
	<script>
		function pingServer() {
			$.ajax({ url: location.href });
		}
		$(document).ready(function() {
			setInterval('pingServer()', 1000 * 60 );
		});
	</script>
	<style>
		.menu > li{
		}
		.clearfix:before, .clearfix:after {
			display: table;
			content: " ";
		}
		.clearfix:after {
			clear: both;
		}
		.glyphicon {
			position: relative;
			top: 1px;
			display: inline-block;
			font-family: 'Glyphicons Halflings';
			-webkit-font-smoothing: antialiased;
			font-style: normal;
			font-weight: normal;
			line-height: 1;
			-moz-osx-font-smoothing: grayscale;
		}
	</style>

	<script>
		$(".menu ul").parents("li").addClass("parent");
		$(".menu li").hover(function(){
			$(this).addClass('hover');
		},function(){
			$(this).removeClass('hover');
		});
	</script>
</head>

<body>

	<div id="container">

		<div id="my_header">

			<div class="div1" style="border-bottom:1px solid #ddd; display: flex; justify-content: flex-start; align-content: center; align-items: center;">

				<div class="div2" style="width: 250px; padding-left: 30px;"><img style=" height: 65px; " src="view/image/logo.png" title="<?php echo $heading_title; ?>" onclick="location = '<?php echo $news; ?>'" /></div>
				<?php if ($logged) { ?>
				<div class="div3" style="width: calc(100% - 250px); text-align: right; padding-right: 30px;">
					<img src="view/image/lock.png" alt="" style="position: relative; top: 3px;" />&nbsp;&nbsp;<?php echo $logged; ?>
					<?php if(isset($this->session->data['admin'])){?> | 開發者模式<?php }?>
				</div>
				<?php } ?>

			</div>

			<?php if ($logged) { ?>
			<ul class="left menu clearfix gradient">

				<li id="banner"><a href="<?php echo $home_banners; ?>">首頁輪播</a></li>

				<li id="news"><a href="<?php echo $news; ?>">關於科林</a></li>

				<li id="list">
					<a class="top">區塊設定</a>
					<ul>
						<li><a href="<?php echo $block1?>">聽力保健</a></li>
						<li><a href="<?php echo $block2?>">讚聲好友</a></li>
					</ul>
				</li>

				<li id="question"><a href="<?php echo $question; ?>">常見QA</a></li>

				<li id="system_setting"><a href="<?php echo $system_setting; ?>">系統設定</a></li>

				<li id="list">
					<a class="top" >後台文件</a>
					<ul>
						<li><a href="http://lazyweb.link/document/T2_basic.pdf" target="_blank">基本規格與操作說明</a></li>
					</ul>
				</li>

				<li id="list">
					<a class="top" >教學文件</a>
					<ul>
						<li><a href="http://lazyweb.link/document/ck_table.pdf" target="_blank">編輯器表格編輯教學</a></li>
						<li><a href="http://lazyweb.link/document/ck_link.pdf" target="_blank">編輯器連結設定教學</a></li>
						<li><a href="http://lazyweb.link/document/ck_video.pdf" target="_blank">編輯器影片崁入教學</a></li>
					</ul>
				</li>

				<?php if( isset($this->session->data['admin']) ) { ?>
					<li id="user">
						<a class="top" ><font color="red">*</font>進階設定</a>
						<ul>
							<li><a href="<?php echo $user_group; ?>">後台人員權限</a></li>
							<li><a href="<?php echo $user; ?>">後台人員帳號管理</a></li>
						</ul>
					</li>
				<?php } ?>

				<li style="float:right"><a class="top" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
				<li id="store" style="float:right"><a href="<?php echo $store; ?>" target="_blank" class="top">前往前台</a>
				</li>
			</ul>
			<?php } ?>
		</div>