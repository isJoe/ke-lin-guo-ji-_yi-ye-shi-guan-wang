<?php echo $header; ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div id="lg_content">
  <div class="box" style="width: 400px; min-height: 300px; margin-top: 40px; margin-left: auto; margin-right: auto;">
    <!--
    <div class="heading">
      <h1><img src="view/image/lockscreen.png" alt="" /> <?php echo $text_login; ?></h1>
    </div>
    -->
    <?php if ($success) { ?>
      <div class="success"><?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="warning"><?php echo $error_warning; ?></div>
      <?php } ?>
      <h4 class="login-title"><i class="fa fa-lock"></i>請輸入您的登入資訊</h4>
    <div class="add-comment styled boxed" id="addcomments">

                                <div class="add-comment-title"><h3>Write a message</h3></div>
                                <div class="comment-form">

                                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                                        <div class="form-inner">
                                            <div class="field_text lightPlaceholder">
                                                <label for="username" class="label_title">帳號</label>
                                                <input type="text" name="username" value="<?php echo $username; ?>"/>
                                            </div>
                                            <div class="field_text field_textarea">
                                                <label for="nicedit-message" class="label_title">密碼</label>
                                                 <input type="password" name="password" value="<?php echo $password; ?>"/>
                                            </div>
                                            <div style="margin-bottom: 15px;" class="g-recaptcha" data-sitekey="6LdzK8YUAAAAAAdhnCqT_WFaYYgbhFVHGi-nYPV7"></div>
                                        </div>
                                        <div class="rowSubmit">
                                            <span class="btn btn-send"><input type="submit" id="send" value="登入" /></span>
                                        </div>
                                    </form>
                                </div>

                            </div>
       <div class="footer-text"><a href="https://www.lazyweb.com.tw" target="_blank">LAZYWeb</a> © <?php echo date('Y') ?> All Rights Reserved.</div>

  </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#form').submit();
	}
});
//--></script>
<!-- Fire Text Editor -->