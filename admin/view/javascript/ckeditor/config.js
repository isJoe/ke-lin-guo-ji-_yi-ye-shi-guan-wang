/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */
CKEDITOR.editorConfig = function( config ) {
	config.toolbar = [
		{name:'edit', items:['Source','Maximize','ShowBlocks','-','Templates']},
		{name:'tool', items:['Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
		{name:'element', items:['NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','Table','HorizontalRule']},
		{name:'link', items:['Link','Unlink']},
		{name:'media', items:['Image','Smiley','SpecialChar','Iframe','Youtube']},
		'/',
		{name:'text', items:['Styles','Format','Font','FontSize','lineheight','TextColor','BGColor']},
		{name:'style', items:['Bold','Italic','Underline','-','CopyFormatting','RemoveFormat','RemoveSpan']},
		{name:'align', items:['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl']}
	];
	config.extraPlugins = 'youtube';
    config.templates_files = ['/admin/view/javascript/ckeditor/plugins/templates/templates/default.js?0605'];
};