<?php
class ControllerHomeHomeBanners extends Controller {
    private $error = array();
    
    private $_mode = FTP_MODE ; 

    public function index() {
        $this->document->setTitle('首頁輪播管理');

        $this->load->model('home/home_banners');

        $this->getList();
    }

    public function update() {

        $this->document->setTitle('首頁輪播管理');

        $this->load->model('home/home_banners');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            //更新banner
            $this->model_home_home_banners->deleteBanner();
            if (!empty($this->request->post['home_banners'])) {
                foreach ($this->request->post['home_banners'] as $key => $row) {
                    $this->model_home_home_banners->addBanner($row);
                }
            }

            $this->session->data['success'] = '更新首頁輪播成功！';

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->redirect($this->url->link('home/home_banners/update', 'token=' . $this->session->data['token'] .$url, 'SSL'));
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } else {
            $this->data['error'] = '';
        }
        $this->getForm();
    }

    protected function getForm() {

        $this->load->model('tool/image');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $this->data['error_name'] = $this->error['name'];
        } else {
            $this->data['error_name'] = '';
        }

        if (isset($this->error['home_banners'])) {
            $this->data['error_home_banners'] = $this->error['home_banners'];
        } else {
            $this->data['error_home_banners'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }


        $this->data['action'] = $this->url->link('home/home_banners/update', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $filterData = array(
            'sort'  => 'sort_order',
            'order' => 'ASC'
        );
        $banner_info = $this->model_home_home_banners->getBannerList($filterData);

        $this->data['token'] = $this->session->data['token'];

        $this->load->model('tool/image');

        if (isset($this->request->post['home_banners'])) {
            $home_banners = $this->request->post['home_banners'];
        } elseif ($banner_info) {
            $home_banners = $banner_info;
        } else {
            $home_banners = array();
        }

        $this->data['banner_info'] = array();

        foreach ($home_banners as $row) {
            if  ( $row['image'] != '' ) {
                $thumb = $this->model_tool_image->resize($row['image'], 100, 100);
            } else {
                $thumb = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }
            if ( $row['image_mobile'] != '' ) {
                $thumb_mobile = $this->model_tool_image->resize($row['image_mobile'], 100, 100);
            } else {
                $thumb_mobile = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }

            $this->data['banner_info'][] = array(
                'image'        => $row['image'],
                'image_mobile' => $row['image_mobile'],
                'title'        => $row['title'],
                'sub_title'    => $row['sub_title'],
                'link'         => $row['link'],
                'link_text'    => $row['link_text'],
                'sort_order'   => $row['sort_order'],
                'status'       => $row['status'],
                'thumb'        => $thumb,
                'thumb_mobile' => $thumb_mobile
            );
        }

        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

        if ( $this->_mode == '1' ) {
            $this->data['_image'] = 'index.php?route=common/filemanager/image&token=' . $this->session->data['token'];          
        } else {
            $this->data['_image'] = 'index.php?route=common/filemanager/curl_switch&token=' . $this->session->data['token'] . '&behavior=image' ; 
        }         

        $this->template = 'home/home_banners_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'home/home_banners')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if( !empty($this->request->post['home_banners']) && count($this->request->post['home_banners']) > 0){
            foreach ( $this->request->post['home_banners'] as $key => $row) {
                if( utf8_strlen($row['image']) < 1 ){
                    $this->error['home_banners'][$key]['image'] = '請上傳圖片';
                }
                if( utf8_strlen($row['image_mobile']) < 1 ){
                    $this->error['home_banners'][$key]['image_mobile'] = '請上傳手機圖片';
                }
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = '請檢查欄位是否填寫正確！';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
?>