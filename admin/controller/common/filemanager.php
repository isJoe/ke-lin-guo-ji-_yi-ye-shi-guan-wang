<?php
class ControllerCommonFileManager extends Controller {
	private $error = array();

	private $_mode = FTP_MODE ; 

	public function index() {

		$this->language->load('common/filemanager');

		$this->data['title'] = $this->language->get('heading_title');

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}

		$this->data['entry_folder'] = $this->language->get('entry_folder');
		$this->data['entry_move'] = $this->language->get('entry_move');
		$this->data['entry_copy'] = $this->language->get('entry_copy');
		$this->data['entry_rename'] = $this->language->get('entry_rename');

		$this->data['button_folder'] = $this->language->get('button_folder');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_move'] = $this->language->get('button_move');
		$this->data['button_copy'] = $this->language->get('button_copy');
		$this->data['button_rename'] = $this->language->get('button_rename');
		$this->data['button_upload'] = $this->language->get('button_upload');
		$this->data['button_refresh'] = $this->language->get('button_refresh');
		$this->data['button_submit'] = $this->language->get('button_submit');

		$this->data['error_select'] = $this->language->get('error_select');
		$this->data['error_directory'] = $this->language->get('error_directory');

		$this->data['token'] = $this->session->data['token'];

		

		$this->load->model('tool/image');

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);

		if (isset($this->request->get['field'])) {
			$this->data['field'] = $this->request->get['field'];
		} else {
			$this->data['field'] = '';
		}

		if (isset($this->request->get['CKEditorFuncNum'])) {
			$this->data['fckeditor'] = $this->request->get['CKEditorFuncNum'];
		} else {
			$this->data['fckeditor'] = false;
		}

		$this->data['directory'] = HTTPS_CATALOG . 'image/data/';
		$this->data['_image'] = 'index.php?route=common/filemanager/image&token=' . $this->session->data['token']; 
		$this->data['_directory'] = 'index.php?route=common/filemanager/directory&token=' . $this->session->data['token']; 
		$this->data['_files'] = 'index.php?route=common/filemanager/files&token=' . $this->session->data['token']; 
		$this->data['_create'] = 'index.php?route=common/filemanager/create&token=' . $this->session->data['token']; 
		$this->data['_delete'] = 'index.php?route=common/filemanager/delete&token=' . $this->session->data['token']; 
		$this->data['_move'] = 'index.php?route=common/filemanager/move&token=' . $this->session->data['token']; 
		$this->data['_copy'] = 'index.php?route=common/filemanager/copy&token=' . $this->session->data['token']; 
		$this->data['_folders'] = 'index.php?route=common/filemanager/folders&token=' . $this->session->data['token']; 
		$this->data['_rename'] = 'index.php?route=common/filemanager/rename&token=' . $this->session->data['token']; 

		$this->data['_mode'] = $this->_mode ;

		$this->template = 'common/filemanager.tpl';

		$this->response->setOutput($this->render());
	}

	public function image() {
		$this->load->model('tool/image');

		if (isset($this->request->get['image'])) {
			echo HTTP_CATALOG . 'image/' . $this->request->get['image'] ;
		}
	}

	public function directory() {
		$json = array();

		if (isset($this->request->post['directory'])) {
			$directories = glob(rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['directory']), '/') . '/*', GLOB_ONLYDIR);

			if ($directories) {
				$i = 0;

				foreach ($directories as $directory) {
					$json[$i]['data'] = basename($directory);
					$json[$i]['attributes']['directory'] = utf8_substr($directory, strlen(DIR_IMAGE . 'data/'));

					$children = glob(rtrim($directory, '/') . '/*', GLOB_ONLYDIR);

					if ($children)  {
						$json[$i]['children'] = ' ';
					}

					$i++;
				}
			}
		}

		$this->response->setOutput(json_encode($json));
	}

	public function files() {
		$json = array();

		if (!empty($this->request->post['directory'])) {
			$directory = DIR_IMAGE . 'data/' . str_replace('../', '', $this->request->post['directory']);
		} else {
			$directory = DIR_IMAGE . 'data/';
		}

		$allowed = array(
			'.jpg',
			'.jpeg',
			'.png',
			'.gif'
		);

		$files = glob(rtrim($directory, '/') . '/*');

		if ($files) {
			foreach ($files as $file) {
				if (is_file($file)) {
					$ext = strrchr($file, '.');
				} else {
					$ext = '';
				}

				if (in_array(strtolower($ext), $allowed)) {
					$size = filesize($file);

					$i = 0;

					$suffix = array(
						'B',
						'KB',
						'MB',
						'GB',
						'TB',
						'PB',
						'EB',
						'ZB',
						'YB'
					);

					while (($size / 1024) > 1) {
						$size = $size / 1024;
						$i++;
					}

					$json[] = array(
						'filename' => basename($file),
						'file'     => utf8_substr($file, utf8_strlen(DIR_IMAGE . 'data/')),
						'size'     => round(utf8_substr($size, 0, utf8_strpos($size, '.') + 4), 2) . $suffix[$i]
					);
				}
			}
		}

		$this->response->setOutput(json_encode($json));
	}	

	public function folders() {
		$this->response->setOutput($this->recursiveFolders(DIR_IMAGE . 'data/'));
	}

	protected function recursiveFolders($directory) {
		$output = '';

		$output .= '<option value="' . utf8_substr($directory, strlen(DIR_IMAGE . 'data/')) . '">' . utf8_substr($directory, strlen(DIR_IMAGE . 'data/')) . '</option>';

		$directories = glob(rtrim(str_replace('../', '', $directory), '/') . '/*', GLOB_ONLYDIR);

		foreach ($directories  as $directory) {
			$output .= $this->recursiveFolders($directory);
		}

		return $output;
	}

	public function upload_file () {

		$json = array();
		$json['status'] = 0 ;
		$json['debug'] = $this->request->files ; 		

		if ( isset($this->request->files['fileToUpload']) || isset($this->request->files['upload']) ) {

			if ( isset($this->request->files['fileToUpload']) ) {
				$files_content = $this->request->files['fileToUpload'] ;
				$is_ckeditor = 'N';
			} else if ( isset($this->request->files['upload']) ) { // 從 CK編輯器上傳來的
				$files_content = $this->request->files['upload'] ;
				$is_ckeditor = 'Y';
			}

			if ( $files_content['error'] == '0' ) {
				$directory = DIR_IMAGE . 'data' ;

				$new_filename = time() . '_' . $files_content['name'] ;

				if ( !file_exists($directory . '/' . $new_filename) ) {
					$file_extension = pathinfo($files_content['name'],PATHINFO_EXTENSION);
					$json['file_extension'] = $file_extension ; 

					## 檢查檔案類型
					$allowed_type = array(
						'image/jpeg',
						'image/pjpeg',
						'image/png',
						'image/x-png',
						'image/gif',
					);
					$allowed_extension = array(
						'jpg',
						'jpeg',
						'gif',
						'png',
					);				
					## 檢查檔案命名規則
					$name_rule_1 = ( str_replace(' ', '' , $new_filename ) == $new_filename ) ? 1 : 0 ; // 半形空白
					$name_rule_2 = ( str_replace('　', '' , $new_filename ) == $new_filename ) ? 1 : 0 ; // 半形空白

					if ( $name_rule_1 == 1 && $name_rule_2 == 1 ) {
						if ( in_array($files_content['type'],$allowed_type) && in_array($file_extension,$allowed_extension) ) {
							
							if ( @move_uploaded_file( $files_content['tmp_name'] , $directory . '/' . $new_filename ) ) {

								if ( $this->_mode == '1' ) {
									$json['status'] = 1 ;
									$json['path'] = 'data/'.$new_filename;
									$json['url']  = HTTP_CATALOG . 'image/data/'.$new_filename;
								} else {
									$local_file  = $directory . '/' . $new_filename ; // 主機上的圖片位置
									$remote_file = SYNC_PROJECT . '/image/data/' . $new_filename ; // FTP 上面的圖片位置

									$json['local_file'] = $local_file ;
									$json['remote_file'] = $remote_file ;

									$upload_to_ftp = $this->upload_by_ftp( $remote_file , $local_file ) ;

									if ( $upload_to_ftp === TRUE ) {
										$json['status'] = '1' ;
										$json['path'] = 'data/'.$new_filename;
										$json['url']  = SYNC_HTTPS . 'image/data/'.$new_filename;								
									} else {
										@unlink($local_file);
										$json['error'] = '上傳失敗(remote)' ;	
									}
								}

							} else {
								$json['error'] = '上傳失敗(local)' ;		
							}
						} else {
							$json['error'] = '上傳檔案類型，不符合規定' ;	
						}
					} else {
						$json['error'] = '上傳失敗，檔案命名不能有空白' ;		
					}
				} else {
					$json['error'] = '該檔案檔名已經被使用過，請重新命名之後再上傳';
				}
			} else {
				$json['error'] = '上傳失敗，或者檔案大小過大' ;
			}

		} else {
			$json['error'] = '上傳失敗，或者檔案大小過大' ;
		}

		if ( isset($this->request->get['CKEditorFuncNum']) && isset($json['url']) ) {
			$funcNum = $this->request->get['CKEditorFuncNum'];
			$fileUrl = $json['url'] ;
			echo "<script>window.parent.CKEDITOR.tools.callFunction( '".$funcNum ."' , '".$fileUrl."' )</script>";
		} else {
			$this->response->setOutput(json_encode($json));	
		}	    
	}

	private function upload_by_ftp ( $remote = '' , $local = '' ) {
		$conn_id = ftp_connect(SYNC_FTP_HOST,"21","2");

		// login with username and password
		$login_result = ftp_login($conn_id, SYNC_FTP_ACCOUNT, SYNC_FTP_PASSWORD);
		
		ftp_pasv($conn_id, true);

		// upload a file
		$status = ftp_put($conn_id, $remote, $local, FTP_BINARY) ;
		
		// close the connection
		ftp_close($conn_id);

		return $status ;		
	}
}
?>