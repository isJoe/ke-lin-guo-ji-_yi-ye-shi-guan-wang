<?php
class ControllerCatalogSystemSetting extends Controller {
    private $error = array();

    public function index() {
        $this->document->setTitle('系統設定 管理');

        $this->load->model('catalog/system_setting');

        $this->update();
    }

    public function update() {

        $this->document->setTitle('系統設定 管理');

        $this->load->model('catalog/system_setting');

        if ( ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm() ) {

            $this->model_catalog_system_setting->updateData($this->request->post);

            $this->session->data['success'] = '更新 系統設定 成功！';

            $this->redirect($this->url->link('catalog/system_setting/update', 'token=' . $this->session->data['token'] , 'SSL'));
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];

            unset($this->session->data['error']);
        } else {
            $this->data['error'] = '';
        }
        $this->getForm();
    }

    protected function getForm() {

        $this->load->model('tool/image');
        $this->load->helper('dev_tool');

        $this->data['action'] = $this->url->link('catalog/system_setting/update', 'token=' . $this->session->data['token'] , 'SSL');

        $system_setting_info = $this->model_catalog_system_setting->getSystemSettingInfo();

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        ## 核心欄位
        $field_list = array(
            'image' , 'image_mb' , 'head_after' , 'head_before' , 'body_before' , 
            'home_block_0_status' , 
            'home_block_1' , 'home_block_1_status' , 
            'home_block_2' , 'home_block_2_status' , 
            'home_block_3' , 'home_block_3_status' , 
            'home_block_4' , 'home_block_4_status' , 
            'home_block_5' , 'home_block_5_status' , 
            'home_block_6' , 'home_block_6_status' , 
        );

        foreach ( $field_list as $field ) {
            if ( isset($this->request->post[$field]) ) {
                $this->data[$field] = $this->request->post[$field];
            } else if ( isset($system_setting_info[$field]) ) {
                $this->data[$field] = $system_setting_info[$field];
            } else {
                $this->data[$field] = '';
            }

            if ( $field == 'image' ) {
                if ( $this->data['image'] != '' ) {
                    $this->data['image_url'] = $this->model_tool_image->resize($this->data['image'], 100, 100);
                } else {
                    $this->data['image_url'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
                }
            }
            if ( $field == 'image_mb' ) {
                if ( $this->data['image_mb'] != '' ) {
                    $this->data['image_mb_url'] = $this->model_tool_image->resize($this->data['image_mb'], 100, 100);
                } else {
                    $this->data['image_mb_url'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
                }
            }
            $this->data['error_'.$field] = _setErrorMessageObject($this->error,$field);
        }        

        ## 其他補助欄位
        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        $this->data['token'] = $this->session->data['token'];
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');

        $this->template = 'catalog/system_setting_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    protected function validateForm() {

        if ( !isset($this->request->post['image']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['image_mb']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['head_after']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['head_before']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['body_before']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['home_block_1']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['home_block_2']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_3']) ) {
            die('參數遺失，請重新操作');      
        } else if ( !isset($this->request->post['home_block_4']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_5']) ) {
            die('參數遺失，請重新操作');
        } else if ( !isset($this->request->post['home_block_6']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_1_status']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_2_status']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_3_status']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_4_status']) ) {
            die('參數遺失，請重新操作'); 
        } else if ( !isset($this->request->post['home_block_5_status']) ) {
            die('參數遺失，請重新操作');             
        } else if ( !isset($this->request->post['home_block_6_status']) ) {
            die('參數遺失，請重新操作');                         
        } else {

            $seq_list[1] = 0 ;
            $seq_list[2] = 0 ;
            $seq_list[3] = 0 ;
            $seq_list[4] = 0 ;
            $seq_list[5] = 0 ;
            $seq_list[6] = 0 ;

            # 步驟 1 : 先把對應的數值內容填入陣列當作
            foreach ( $seq_list as $seq => $val ) {
                if ( $this->request->post['home_block_1'] == $seq ) {
                    $seq_list[$seq] = $seq_list[$seq] + 1 ;
                }
                if ( $this->request->post['home_block_2'] == $seq ) {
                    $seq_list[$seq] = $seq_list[$seq] + 1 ;
                }
                if ( $this->request->post['home_block_3'] == $seq ) {
                    $seq_list[$seq] = $seq_list[$seq] + 1 ;
                }
                if ( $this->request->post['home_block_4'] == $seq ) {
                    $seq_list[$seq] = $seq_list[$seq] + 1 ;
                }
                if ( $this->request->post['home_block_5'] == $seq ) {
                    $seq_list[$seq] = $seq_list[$seq] + 1 ;
                }
                if ( $this->request->post['home_block_6'] == $seq ) {
                    $seq_list[$seq] = $seq_list[$seq] + 1 ;
                }
            }

            # 步驟 2 : 檢視陣列的內容,並檢查之(都必須為1)
            foreach ( $seq_list as $index => $val ) {
                if ( $val != '1' ) {
                    $this->error['home_block_'.$index] = '順序數值不能重複';
                }
            }
        }

        if (!$this->error) {
            return true;
        } else {
            $this->error['warning'] = '請檢查欄位是否填寫正確!';
            return false;
        }
    }

}
?>