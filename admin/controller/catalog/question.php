<?php
class ControllerCatalogQuestion extends Controller {
    private $error = array();

    private $_mode = FTP_MODE ;

    public function index() {
        if ( !$this->user->hasPermission('access','catalog/question') ) {
            $this->redirect($this->url->link('error/permission', 'token=' . $this->session->data['token'] , 'SSL'));
        }
        $this->document->setTitle('常見問題 管理');
        $this->data['text_action_page_title'] = '常見問題 列表';
        $this->load->model('catalog/question');
        $this->load->helper('dev_tool');
        $this->getList();
    }

    public function insert() {
        $this->document->setTitle('新增 常見問題');
        $this->load->model('tool/image');
        $this->load->model('catalog/question');
        $this->load->helper('dev_tool');
        $this->data['text_action_page_title'] = '新增 常見問題';

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm() ) {

            $id = $this->model_catalog_question->addQuestion($this->request->post);

            $this->session->data['success'] = '新增 常見問題 成功！';

            $url = _filterURIparameter(array('token','route','id','stillEdit'));

            if ( isset($this->request->get['stillEdit']) ) {
                $this->redirect($this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&' . $url . '&id=' . $id , 'SSL'));
            } else {
                $this->redirect($this->url->link('catalog/question/', 'token=' . $this->session->data['token'] . '&' . $url , 'SSL'));
            }
        }
        $this->getForm();
    }

    public function update() {
        $this->document->setTitle('編輯 常見問題');
        $this->load->model('tool/image');
        $this->load->model('catalog/question');
        $this->load->helper('dev_tool');
        $this->data['text_action_page_title'] = '編輯 常見問題';
        if ( ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm() && isset($this->request->get['id'])  ) {

            $id = $this->request->get['id'];
            $this->model_catalog_question->editQuestion($id, $this->request->post);

            $this->session->data['success'] = '修改 常見問題 成功！';
            $url = _filterURIparameter(array('token','route','id','stillEdit')) ;

            if ( isset($this->request->get['stillEdit']) ) {
                $this->redirect($this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&' . $url . '&id=' . $id , 'SSL'));
            } else {
                $this->redirect($this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&' . $url, 'SSL'));
            }
        }
        $this->getForm();
    }

    public function delete() {
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/question');
        $this->load->helper('dev_tool');
        if (isset($this->request->post['selected'])) {

            foreach ($this->request->post['selected'] as $id) {
                $this->model_catalog_question->deleteQuestionInfo($id);
            }

            $this->session->data['success'] = '刪除 常見問題 成功！';

            //拿取所有的get參數
            $currentUrl = _getAssignUrlParameterValue(array('page'));
            $currentUrl = ( $currentUrl == '' ) ? '&' : '&' . $currentUrl ;

            $this->redirect($this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $currentUrl, 'SSL'));

        }

        $this->getList();
    }

    protected function getList() {


        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        //拿取所有的get參數
        $currentUrl = _getAssignUrlParameterValue(array('page'));
        $currentUrl = ( $currentUrl == '' ) ? '&' : '&' . $currentUrl ;

        $this->data['insert'] = $this->url->link('catalog/question/insert', 'token=' . $this->session->data['token'] . $currentUrl, 'SSL');
        $this->data['delete'] = $this->url->link('catalog/question/delete', 'token=' . $this->session->data['token'] . $currentUrl, 'SSL');

        $data = array(
            'sort'  => "sort_order",
            'order' => "ASC",
            'start' => ($page - 1) * $this->config->get('config_admin_limit'),
            'limit' => $this->config->get('config_admin_limit')
        );

        $this->load->model('tool/image');

        $results_total = $this->model_catalog_question->getQuestionTotal($data);
        $results       = $this->model_catalog_question->getQuestion($data);

        $this->data['lecture_list'] = array();
        foreach ($results as $key => $row) {
            $action = array();
            $action[] = array(
                'text' => '編輯',
                'href' => $this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&id=' . $row['id'] . $currentUrl, 'SSL')
                );

            $results[$key]['action'] = $action;

            if ( $results[$key]['image'] != '' ) {
                $results[$key]['image_url'] = $this->model_tool_image->resize($row['image'], 100, 100);
            } else {
                $results[$key]['image_url'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
            }

            if ($row['status'] == 1) {
                $results[$key]['status'] = "<span>上架</span>";
            }else{
                $results[$key]['status'] = "<span style='color:red'>關閉</span>";
            }

            $this->data['question_list'] = $results;
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        // 給排序拼裝用的網址
        $sortOrderUrl = _getAssignUrlParameterValue(array('page'));

        $pagination = new Pagination();
        $pagination->total = $results_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_admin_limit');
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&page={page}', 'SSL');

        $this->data['pagination'] = $pagination->render();

        $this->data['token'] = $this->session->data['token'];
        $this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->template = 'catalog/question_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
            );


        $this->response->setOutput($this->render());
    }

    protected function getForm() {

        $action_url = _filterURIparameter(array('token','route')) ;

        if (!isset($this->request->get['id'])) {
            $this->data['action'] = $this->url->link('catalog/question/insert', 'token=' . $this->session->data['token'] . '&' .$action_url , 'SSL');
        } else {
            $this->data['action'] = $this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&' .$action_url , 'SSL');
        }
        $this->data['cancel'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&' . _filterURIparameter(array('token','route','id')) , 'SSL');

        if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $questionInfo = $this->model_catalog_question->getQuestionInfo($this->request->get['id']);
        }

        $this->data['error_warning']     = _setErrorMessageObject($this->error,'warning');
        $this->data['error_image']       = _setErrorMessageObject($this->error,'image');
        $this->data['error_title']       = _setErrorMessageObject($this->error,'title');
        $this->data['error_intro']       = _setErrorMessageObject($this->error,'intro');
        $this->data['error_description'] = _setErrorMessageObject($this->error,'description');
        $this->data['error_status']      = _setErrorMessageObject($this->error,'status');
        $this->data['error_sort_order']  = _setErrorMessageObject($this->error,'sort_order');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        //常見問題 封面小圖
        if (isset($this->request->post['image'])) {
            $this->data['image'] = $this->request->post['image'];
        } elseif (!empty($questionInfo)) {
            $this->data['image'] = $questionInfo['image'];
        } else {
            $this->data['image'] = '';
        }

        if ( $this->data['image'] != '' ) {
            $this->data['image_url'] = $this->model_tool_image->resize($this->data['image'], 100, 100);
        } else {
            $this->data['image_url'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        //常見問題 內文大圖
        if (isset($this->request->post['image2'])) {
            $this->data['image2'] = $this->request->post['image2'];
        } elseif (!empty($questionInfo)) {
            $this->data['image2'] = $questionInfo['image2'];
        } else {
            $this->data['image2'] = '';
        }

        if ( $this->data['image2'] != '' ) {
            $this->data['image_url2'] = $this->model_tool_image->resize($this->data['image2'], 100, 100);
        } else {
            $this->data['image_url2'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        // 常見問題 標題
        if (isset($this->request->post['title'])) {
            $this->data['title'] = $this->request->post['title'];
        } elseif (!empty($questionInfo)) {
            $this->data['title'] = $questionInfo['title'];
        } else {
            $this->data['title'] = '';
        }

        // 常見問題 簡介
        if (isset($this->request->post['intro'])) {
            $this->data['intro'] = $this->request->post['intro'];
        } elseif (!empty($questionInfo)) {
            $this->data['intro'] = $questionInfo['intro'];
        } else {
            $this->data['intro'] = '';
        }

        // 常見問題 詳細內容
        if (isset($this->request->post['description'])) {
            $this->data['description'] = $this->request->post['description'];
        } elseif (!empty($questionInfo)) {
            $this->data['description'] = $questionInfo['description'];
        } else {
            $this->data['description'] = '';
        }

        // 常見問題 狀態
        if (isset($this->request->post['status'])) {
            $this->data['status'] = $this->request->post['status'];
        } elseif (!empty($questionInfo)) {
            $this->data['status'] = $questionInfo['status'];
        } else {
            $this->data['status'] = 1;
        }

        // 常見問題 排序
        if (isset($this->request->post['sort_order'])) {
            $this->data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($questionInfo)) {
            $this->data['sort_order'] = $questionInfo['sort_order'];
        } else {
            $this->data['sort_order'] = 0;
        }        

        // 預設沒圖片時
        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        // Token 參數
        $this->data['token'] = $this->session->data['token'];
        // 圖檔管理要用的參數
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');

        if ( $this->_mode == '1' ) {
            $this->data['_image'] = 'index.php?route=common/filemanager/image&token=' . $this->session->data['token'];
        } else {
            $this->data['_image'] = 'index.php?route=common/filemanager/curl_switch&token=' . $this->session->data['token'] . '&behavior=image' ;
        }

        if ( isset($this->request->get['show_data'])) {
            echo '<pre>'; print_r($this->data);
        } else {

            $this->template = 'catalog/question_form.tpl';
            $this->children = array(
                'common/header',
                'common/footer'
                );

            $this->response->setOutput($this->render());
        }
    }

    protected function validateForm() {

        if( empty($this->request->post['image']) ){
            $this->error['image'] = '請上傳 封面小圖';
        }

        if( utf8_strlen($this->request->post['title']) < 1 ){
            $this->error['title'] = '請填寫 標題';
        }

        if( empty($this->request->post['intro']) ){
            $this->error['intro'] = '請填寫 簡介';
        }

        if( utf8_strlen($this->request->post['description']) < 1 ){
            $this->error['description'] = '請填寫 詳細內容';
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = '請檢查欄位是否填寫正確！';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
?>