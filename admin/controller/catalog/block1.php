<?php
class ControllerCatalogBlock1 extends Controller {
    private $error = array();

    private $_mode = FTP_MODE ;

    public function index() {
        $this->document->setTitle('聽力保健管理');

        $this->load->model('catalog/block');

        $this->getList();
    }

    public function update() {

        $this->document->setTitle('聽力保健管理');
        $this->data['text_action_page_title'] = '聽力保健 管理';

        $this->load->model('catalog/block');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $block_id = $this->request->post['block_id'];

            $this->model_catalog_block->deleteBlockImage($block_id);
            $this->model_catalog_block->addBlockImage($block_id, $this->request->post['image'], $this->request->post['image2']);

            $this->model_catalog_block->deleteBlock($block_id);
            if (!empty($this->request->post['block'])) {
                foreach ($this->request->post['block'] as $sort_order => $row) {
                    $this->model_catalog_block->addBlock($block_id, $sort_order, $row);
                }
            }

            $this->session->data['success'] = '更新聽力保健成功！';

            $this->redirect($this->url->link('catalog/block1/update', 'token=' . $this->session->data['token'], 'SSL'));
        }
        if (isset($this->session->data['success'])) {
            $this->data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $this->data['success'] = '';
        }

        if (isset($this->session->data['error'])) {
            $this->data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $this->data['error'] = '';
        }
        $this->getForm();
    }

    protected function getForm() {

        $this->load->model('tool/image');

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        if (isset($this->error['block_id'])) {
            $this->data['error_block_id'] = $this->error['block_id'];
        } else {
            $this->data['error_block_id'] = '';
        }

        if (isset($this->error['image'])) {
            $this->data['error_image'] = $this->error['image'];
        } else {
            $this->data['error_image'] = '';
        }

        if (isset($this->error['image2'])) {
            $this->data['error_image2'] = $this->error['image2'];
        } else {
            $this->data['error_image2'] = '';
        }

        if (isset($this->error['block'])) {
            $this->data['error_block'] = $this->error['block'];
        } else {
            $this->data['error_block'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $this->data['action'] = $this->url->link('catalog/block1/update', 'token=' . $this->session->data['token'] . $url, 'SSL');

        $block_id = '1';
        $this->data['block_id'] = $block_id;

        $filterData = array(
            'block_id' => $block_id,
            'sort'     => 'sort_order',
            'order'    => 'ASC'
        );
        $block_info  = $this->model_catalog_block->getBlockList($filterData);
        $block_image = $this->model_catalog_block->getBlockImage($filterData);

        $this->data['token'] = $this->session->data['token'];

        $this->load->model('tool/image');

        if (isset($this->request->post['block'])) {
            $block = $this->request->post['block'];
        } elseif ($block_info) {
            $block = $block_info;
        } else {
            $block = array();
        }

        if (isset($this->request->post['image'])) {
            $this->data['image'] = $this->request->post['image'];
        } elseif (!empty($block_image)) {
            $this->data['image'] = $block_image['image'];
        } else {
            $this->data['image'] = '';
        }

        if ( $this->data['image'] != '' ) {
            $this->data['image_url'] = $this->model_tool_image->resize($this->data['image'], 100, 100);
        } else {
            $this->data['image_url'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        if (isset($this->request->post['image2'])) {
            $this->data['image2'] = $this->request->post['image2'];
        } elseif (!empty($block_image)) {
            $this->data['image2'] = $block_image['image2'];
        } else {
            $this->data['image2'] = '';
        }

        if ( $this->data['image2'] != '' ) {
            $this->data['image2_url'] = $this->model_tool_image->resize($this->data['image2'], 100, 100);
        } else {
            $this->data['image2_url'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        }

        $this->data['block_info'] = array();

        foreach ($block as $row) {
            $this->data['block_info'][] = array(
                'title' => $row['title'],
                'intro' => $row['intro'],
                'link'  => $row['link'],
            );
        }


        // 預設沒圖片時
        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        // Token 參數
        $this->data['token'] = $this->session->data['token'];
        // 圖檔管理要用的參數
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');

        if ( $this->_mode == '1' ) {
            $this->data['_image'] = 'index.php?route=common/filemanager/image&token=' . $this->session->data['token'];
        } else {
            $this->data['_image'] = 'index.php?route=common/filemanager/curl_switch&token=' . $this->session->data['token'] . '&behavior=image' ;
        }

        $this->template = 'catalog/block_form.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'catalog/block1')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!isset($this->request->post['block_id']) || !is_numeric($this->request->post['block_id'])) {
            $this->error['block_id'] = '資料異常，請重新登入後再嘗試操作';
        }

        if( empty($this->request->post['image']) ){
            $this->error['image'] = '請上傳 背景圖';
        }

        if( empty($this->request->post['image2']) ){
            $this->error['image2'] = '請上傳 透明圖';
        }

        if (isset($this->request->post['block'])) {
            $key = 0;
            foreach ($this->request->post['block'] as $block) {
                if( utf8_strlen($block['title']) < 1 ){
                    $this->error['block'][$key]['title'] = '請填寫 標題';
                }

                if( utf8_strlen($block['intro']) < 1 ){
                    $this->error['block'][$key]['intro'] = '請填寫 簡介';
                }
                $key++;
            }
        }


        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = '請檢查欄位是否填寫正確！';
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}
?>