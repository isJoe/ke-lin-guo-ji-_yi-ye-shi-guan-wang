<?php

$_ = array_merge($_, array(
	'heading_title'		=> "首頁滑動廣告(SliderIB)",
	'text_success'		=> "Success: You have updated SliderIB module configuration!",
	'error_permission'	=> "Warning: You don't have permission to modify module SliderIB!",
	'text_module'		=> "Modules",
	
	'sliderib_entry_text'		=>	"說明",
	'sliderib_entry_text1'		=>	"第1層",
	'sliderib_entry_text2'		=>	"第2層",
	'sliderib_entry_text3'		=>	"第3層 ",
	'sliderib_entry_button'		=>	"按鈕說明",
	'sliderib_entry_button1'	=>	"滑鼠移入前",
	'sliderib_entry_button2'	=>	"滑鼠移入後",
	'sliderib_entry_link'		=>	"超連結",
	'sliderib_entry_image'		=>	"圖片",
	'sliderib_entry_order'		=>	"排序(數字越小越前面)",
	
));
