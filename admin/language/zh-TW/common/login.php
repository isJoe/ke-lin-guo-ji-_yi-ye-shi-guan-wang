<?php
// header
$_['heading_title']  = '登入管理後台';

// Text
$_['text_heading']   = '管理系統(Administration)';
$_['text_login']     = '商店後台管理系統';
$_['text_forgotten'] = '忘記密碼';

// Entry
$_['entry_username'] = '管理員';
$_['entry_password'] = '密碼';

// Button
$_['button_login']   = '登入';

// Error
$_['error_login']    = '管理員名稱或密碼或驗證錯誤.';
$_['error_token']    = 'token session無效,請重新登入';
?>
