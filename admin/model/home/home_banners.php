<?php
class ModelHomeHomeBanners extends Model {

    // 新增 首頁輪播 資訊
    public function addBanner ( $data=array() ) {

        $home_banners = DB_PREFIX . 'home_banners';

        $image        = $this->db->escape($data['image']);
        $image_mobile = $this->db->escape($data['image_mobile']);
        $title        = $this->db->escape($data['title']);
        $sub_title    = $this->db->escape($data['sub_title']);
        $link         = $this->db->escape($data['link']);
        $link_text    = $this->db->escape($data['link_text']);
        $sort_order   = $this->db->escape($data['sort_order']);
        $status       = $this->db->escape($data['status']);

        $sql = " INSERT INTO {$home_banners} SET
            image        = '{$image}',
            image_mobile = '{$image_mobile}',
            title        = '{$title}',
            sub_title    = '{$sub_title}',
            link         = '{$link}',
            link_text    = '{$link_text}',
            sort_order   = '{$sort_order}',
            status       = '{$status}',
            created_at   = now()
        ";

        $this->db->query($sql);
        $id = $this->db->getLastId();

        return $id;
    }

    //刪除 所有首頁輪播 資訊
    public function deleteBanner () {
        $home_banners = DB_PREFIX . 'home_banners' ;
        $sql     = "TRUNCATE {$home_banners}";
        $this->db->query($sql);
    }

    // 取得 首頁輪播 資訊
    public function getBannerList ($data=array()) {

        $home_banners      = DB_PREFIX . 'home_banners';

        $sql = " SELECT * FROM {$home_banners}";

        ###sort order排序###
        $sort  = $this->db->escape($data['sort']);
        $order = $this->db->escape($data['order']);

        $sql .= " ORDER BY " . $sort . " " . $order ;
        if (isset($data['start']) && isset($data['limit'])) {
            $start = $this->db->escape($data['start']);
            $limit = $this->db->escape($data['limit']);

            $sql .= " LIMIT " . $start . "," . $limit ;
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }
}
?>
