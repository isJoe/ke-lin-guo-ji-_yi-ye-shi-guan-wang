<?php
class ModelCatalogQuestion extends Model {

    public function deleteQuestionInfo ( $id='' ) {
        $question = DB_PREFIX . 'question' ;
        $id = $this->db->escape($id);
        $sql = " DELETE FROM {$question} WHERE id = '{$id}' ";
        $this->db->query($sql);
    }

    public function addQuestion ( $data = array() ) {

        $question = DB_PREFIX . 'question';

        $image          = $this->db->escape($data['image']);
        $image2         = $this->db->escape($data['image2']);
        $title          = $this->db->escape($data['title']);
        $intro          = $this->db->escape($data['intro']);
        $description    = $this->db->escape($data['description']);
        $status         = $this->db->escape($data['status']);
        $sort_order     = $this->db->escape($data['sort_order']);

        $sql = " INSERT INTO {$question} SET
            image          = '{$image}',
            image2         = '{$image2}',
            title          = '{$title}',
            intro          = '{$intro}',
            description    = '{$description}',
            status         = '{$status}',
            sort_order     = '{$sort_order}',
            created_at     = now(),
            updated_at     = now()
        ";
        $this->db->query($sql);
        $id = $this->db->getLastId();

        return $id;
    }

    public function editQuestion ( $id='' , $data=array() ) {

        $question = DB_PREFIX . 'question';

        $id             = $this->db->escape($id);
        $image          = $this->db->escape($data['image']);
        $image2         = $this->db->escape($data['image2']);
        $title          = $this->db->escape($data['title']);
        $intro          = $this->db->escape($data['intro']);
        $description    = $this->db->escape($data['description']);
        $status         = $this->db->escape($data['status']);
        $sort_order     = $this->db->escape($data['sort_order']);

        $sql = " UPDATE {$question} SET
            image          = '{$image}',
            image2         = '{$image2}',
            title          = '{$title}',
            intro          = '{$intro}',
            description    = '{$description}',
            status         = '{$status}',
            sort_order     = '{$sort_order}',
            updated_at     = now()
            WHERE id = '{$id}'
        ";

        $this->db->query($sql);
    }

    public function getQuestion ($data=array()) {

        $question = DB_PREFIX . 'question';
        $sql = "SELECT * FROM {$question}";

        if ( isset($data['sort']) && isset($data['order']) ) {
            $sort = $this->db->escape($data['sort']);
            $order = $this->db->escape($data['order']);

            $sql .= " ORDER BY " . $sort . " " . $order ;
        }
        if (isset($data['start']) && isset($data['limit'])) {
            $start = $this->db->escape($data['start']);
            $limit = $this->db->escape($data['limit']);

            $sql .= " LIMIT " . $start . "," . $limit ;
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getQuestionTotal ( $data=array() ) {

        $question = DB_PREFIX . 'question';

        $sql = "SELECT count(1) as sum FROM {$question}";

        $query = $this->db->query($sql);
        return $query->row['sum'];
    }

    public function getQuestionInfo ( $id='' ) {

        $question = DB_PREFIX . 'question';

        $id = $this->db->escape( $id );

        $sql = "SELECT * FROM {$question} WHERE id = '{$id}' ";
        $query = $this->db->query($sql);
        return $query->row;
    }

}
?>
