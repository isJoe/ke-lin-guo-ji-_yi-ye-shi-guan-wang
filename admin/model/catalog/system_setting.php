<?php
class ModelCatalogSystemSetting extends Model {

    public function updateData ( $data = array() ) {
        $system_setting = DB_PREFIX . 'system_setting';
        $image       = $this->db->escape($data['image']);
        $image_mb    = $this->db->escape($data['image_mb']);
        $head_after  = $this->db->escape($data['head_after']);
        $head_before = $this->db->escape($data['head_before']);
        $body_before = $this->db->escape($data['body_before']);

        $home_block_1 = $this->db->escape($data['home_block_1']);
        $home_block_2 = $this->db->escape($data['home_block_2']);
        $home_block_3 = $this->db->escape($data['home_block_3']);
        $home_block_4 = $this->db->escape($data['home_block_4']);
        $home_block_5 = $this->db->escape($data['home_block_5']);
        $home_block_6 = $this->db->escape($data['home_block_6']);

        $home_block_0_status = $this->db->escape($data['home_block_0_status']);
        $home_block_1_status = $this->db->escape($data['home_block_1_status']);
        $home_block_2_status = $this->db->escape($data['home_block_2_status']);
        $home_block_3_status = $this->db->escape($data['home_block_3_status']);
        $home_block_4_status = $this->db->escape($data['home_block_4_status']);
        $home_block_5_status = $this->db->escape($data['home_block_5_status']);
        $home_block_6_status = $this->db->escape($data['home_block_6_status']);

        $sql = "
            UPDATE {$system_setting} SET
                image       = '{$image}',
                image_mb    = '{$image_mb}',
                head_after  = '{$head_after}',
                head_before = '{$head_before}',
                body_before = '{$body_before}',

                home_block_1 = '{$home_block_1}',
                home_block_2 = '{$home_block_2}',
                home_block_3 = '{$home_block_3}',
                home_block_4 = '{$home_block_4}',
                home_block_5 = '{$home_block_5}',
                home_block_6 = '{$home_block_6}',

                home_block_0_status = '{$home_block_0_status}',
                home_block_1_status = '{$home_block_1_status}',
                home_block_2_status = '{$home_block_2_status}',
                home_block_3_status = '{$home_block_3_status}',
                home_block_4_status = '{$home_block_4_status}',
                home_block_5_status = '{$home_block_5_status}',
                home_block_6_status = '{$home_block_6_status}',

                updated_at  = now()
        ";
        $this->db->query($sql);
    }

    public function getSystemSettingInfo () {
        $system_setting  = DB_PREFIX . 'system_setting';
        $sql = " SELECT * FROM {$system_setting} ";
        $query = $this->db->query($sql);
        return $query->row;
    }
}
?>
