<?php
class ModelCatalogNews extends Model {

    public function deleteNewsInfo ( $id='' ) {
        $news = DB_PREFIX . 'news' ;
        $id = $this->db->escape($id);
        $sql = " DELETE FROM {$news} WHERE id = '{$id}' ";
        $this->db->query($sql);
    }

    public function addNews ( $data = array() ) {

        $news = DB_PREFIX . 'news';

        $image          = $this->db->escape($data['image']);
        $image2         = $this->db->escape($data['image2']);
        $title          = $this->db->escape($data['title']);
        $intro          = $this->db->escape($data['intro']);
        $description    = $this->db->escape($data['description']);
        $status         = $this->db->escape($data['status']);
        $sort_order         = $this->db->escape($data['sort_order']);

        $sql = " INSERT INTO {$news} SET
            image          = '{$image}',
            image2         = '{$image2}',
            title          = '{$title}',
            intro          = '{$intro}',
            description    = '{$description}',
            status         = '{$status}',
            sort_order     = '{$sort_order}',
            created_at     = now(),
            updated_at     = now()
        ";
        $this->db->query($sql);
        $id = $this->db->getLastId();

        return $id;
    }

    public function editNews ( $id='' , $data=array() ) {

        $news = DB_PREFIX . 'news';

        $id             = $this->db->escape($id);
        $image          = $this->db->escape($data['image']);
        $image2         = $this->db->escape($data['image2']);
        $title          = $this->db->escape($data['title']);
        $intro          = $this->db->escape($data['intro']);
        $description    = $this->db->escape($data['description']);
        $status         = $this->db->escape($data['status']);
        $sort_order     = $this->db->escape($data['sort_order']);

        $sql = " UPDATE {$news} SET
	        image          = '{$image}',
            image2         = '{$image2}',
	        title          = '{$title}',
	        intro          = '{$intro}',
	        description    = '{$description}',
	        status         = '{$status}',
            sort_order     = '{$sort_order}',
            updated_at     = now()
            WHERE id = '{$id}'
        ";

        $this->db->query($sql);
    }

    public function getNews ($data=array()) {

		$news = DB_PREFIX . 'news';
        $sql = "SELECT * FROM {$news}";

        if ( isset($data['sort']) && isset($data['order']) ) {
            $sort = $this->db->escape($data['sort']);
            $order = $this->db->escape($data['order']);

            $sql .= " ORDER BY " . $sort . " " . $order ;
        }
        if (isset($data['start']) && isset($data['limit'])) {
            $start = $this->db->escape($data['start']);
            $limit = $this->db->escape($data['limit']);

            $sql .= " LIMIT " . $start . "," . $limit ;
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getNewsTotal ( $data=array() ) {

        $news = DB_PREFIX . 'news';

        $sql = "SELECT count(1) as sum FROM {$news}";

        $query = $this->db->query($sql);
        return $query->row['sum'];
    }

    public function getNewsInfo ( $id='' ) {

        $news = DB_PREFIX . 'news';

        $id = $this->db->escape( $id );

        $sql = "SELECT * FROM {$news} WHERE id = '{$id}' ";
        $query = $this->db->query($sql);
        return $query->row;
    }

}
?>
