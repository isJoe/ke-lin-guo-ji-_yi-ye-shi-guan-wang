<?php
class ModelCatalogBlock extends Model {

    public function deleteBlockImage ( $block_id='' ) {
        $block_image    = DB_PREFIX . 'block_image' ;
        $block_id = $this->db->escape($block_id);

        $sql = " DELETE FROM {$block_image} WHERE block_id = '{$block_id}' ";
        $this->db->query($sql);
    }

    public function addBlockImage ( $block_id, $image, $image2 ) {

        $block_image = DB_PREFIX . 'block_image';

        $block_id = $this->db->escape($block_id);
        $image    = $this->db->escape($image);
        $image2   = $this->db->escape($image2);

        $sql = " INSERT INTO {$block_image} SET
            block_id = '{$block_id}',
            image    = '{$image}',
            image2   = '{$image2}'
        ";
        $this->db->query($sql);
        $id = $this->db->getLastId();

        return $id;
    }

    public function deleteBlock ( $block_id='' ) {
        $block    = DB_PREFIX . 'block' ;
        $block_id = $this->db->escape($block_id);

        $sql = " DELETE FROM {$block} WHERE block_id = '{$block_id}' ";
        $this->db->query($sql);
    }

    public function addBlock ( $block_id, $sort_order, $data = array() ) {

        $block = DB_PREFIX . 'block';

        $block_id       = $this->db->escape($block_id);
        $title          = $this->db->escape($data['title']);
        $intro          = $this->db->escape($data['intro']);
        $link           = $this->db->escape($data['link']);
        $sort_order     = $this->db->escape($sort_order);

        $sql = " INSERT INTO {$block} SET
            block_id   = '{$block_id}',
            title      = '{$title}',
            intro      = '{$intro}',
            link       = '{$link}',
            sort_order = '{$sort_order}',
            created_at = now(),
            updated_at = now()
        ";
        $this->db->query($sql);
        $id = $this->db->getLastId();

        return $id;
    }

    public function getBlockList ($data=array()) {

        $block = DB_PREFIX . 'block';
        $sql = "SELECT * FROM {$block} WHERE 1";

        if ( isset($data['block_id']) ) {
            $block_id = $this->db->escape($data['block_id']);
            $sql .= " AND block_id = '{$block_id}'";
        }

        if ( isset($data['sort']) && isset($data['order']) ) {
            $sort = $this->db->escape($data['sort']);
            $order = $this->db->escape($data['order']);

            $sql .= " ORDER BY " . $sort . " " . $order ;
        }
        if (isset($data['start']) && isset($data['limit'])) {
            $start = $this->db->escape($data['start']);
            $limit = $this->db->escape($data['limit']);

            $sql .= " LIMIT " . $start . "," . $limit ;
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getBlockImage ( $data=array() ) {

        $block_image = DB_PREFIX . 'block_image';

        $sql = "SELECT * FROM {$block_image} WHERE 1";

        if ( isset($data['block_id']) ) {
            $block_id = $this->db->escape($data['block_id']);
            $sql .= " AND block_id = '{$block_id}'";
        }

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getBlockInfo ( $id='' ) {

        $block = DB_PREFIX . 'block';

        $id = $this->db->escape( $id );

        $sql = "SELECT * FROM {$block} WHERE id = '{$id}' ";
        $query = $this->db->query($sql);
        return $query->row;
    }

}
?>
