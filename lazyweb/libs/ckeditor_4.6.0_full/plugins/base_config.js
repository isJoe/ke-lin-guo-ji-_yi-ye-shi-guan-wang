/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
		'/',
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'basicstyles', groups: [ 'cleanup', 'basicstyles' ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Save,Preview,Print,NewPage,Templates,Radio,Checkbox,TextField,Textarea,Select,Button,ImageButton,HiddenField,Form,Anchor,Flash,Iframe,Smiley,About,CreateDiv,Language,Replace,Find,SelectAll,Scayt,CopyFormatting,RemoveFormat,Subscript,Superscript,Source,Styles,Format,Font,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,BidiLtr,BidiRtl,Image,Table,HorizontalRule,SpecialChar,PageBreak,Maximize,ShowBlocks,Link,Unlink';
};