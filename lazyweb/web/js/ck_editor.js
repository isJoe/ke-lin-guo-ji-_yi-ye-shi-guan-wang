$(function() {
    $('.cke_editable').find('table').wrap('<div class="cke_editable__table--scroll"></div>');
    $('.cke_editable').find('a').attr('target', '_blank');
});