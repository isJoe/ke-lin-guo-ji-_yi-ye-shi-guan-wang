$(function() {
    // NOTE:Header detect
    if ( $('.header').length > 0 ) {
        $(window).on('scroll resize', function() {
            let now_position = $(window).scrollTop();

            if ( !$('.header').hasClass('header--active') ) {
                if ( now_position > 60 ) {
                    $('.header').addClass('header--active');
                }
            } else {
                if ( now_position < 1 ) {
                    $('.header').removeClass('header--active');
                }
            }
        });

        $('.header-menu__burger').on('click', function() {
            $('.header-menu__nav').addClass('header-menu__nav--open');
            $('.header-menu__close').addClass('header-menu__close--open');
        });

        $('.header-menu__close').on('click', function() {
            $('.header-menu__nav').removeClass('header-menu__nav--open');
            $('.header-menu__close').removeClass('header-menu__close--open');
        });

        $('.header-menu__btn').on('click', function() {
            let content_index = $(this).index();
            let scroll_position = $('[data-target]').eq(content_index).offset().top - $('.header-menu').height();

            $('html, body').animate({
                scrollTop: scroll_position
            }, 300);

            if ( $(window).width() < 992 ) {
                $('.header-menu__close').trigger('click');
            }
        });

        $('#go2contact').on('click', function() {
            let scroll_position = $('#contact').offset().top - $('.header-menu').height();

            $('html, body').animate({
                scrollTop: scroll_position
            }, 300);
        });

        $(window).on('scroll resize', function() {
            let win_mid = $(window).scrollTop() + $(window).height() * 0.5;
            let is_active = false;

            $.each($('[data-target]'), function() {
                let ele_top = $(this).offset().top;
                let ele_bottom = $(this).offset().top + $(this).height();
                let anchor_index = $(this).index() - 2;

                if ( ele_top < win_mid && win_mid < ele_bottom ) {
                    $('.header-menu__btn')
                        .removeClass('header-menu__btn--active')
                        .eq(anchor_index)
                        .addClass('header-menu__btn--active');

                    is_active = true;
                }
            });

            if ( !is_active ) {
                $('.header-menu__btn').removeClass('header-menu__btn--active')
            }
        });
    }

    // NOTE:Go to top
    if ( $('.gototop').length > 0 ) {
        $(window).on('scroll resize', function() {
            let w_top = $(window).scrollTop(),
                w_height = $(window).height() * 0.8;

            if ( w_top > w_height ) {
                if ( !$('.gototop').hasClass('gototop--active') ) {
                    $('.gototop').addClass('gototop--active');
                    $('.gototop').stop().fadeIn(400);
                }
            } else {
                if ( $('.gototop').hasClass('gototop--active') ) {
                    $('.gototop').removeClass('gototop--active');
                    $('.gototop').stop().fadeOut(400);
                }
            }
        });

        $('.gototop').on('click', function() {
            $('html, body').animate({
                scrollTop: 0
            }, 300);
        });
    }

    // NOTE:Home slick
    if ( $('.home-slider').length > 0 ) {
        $('.home-slider__slick').slick({
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3500,
            speed: 400,
            pauseOnHover:false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        dots: true,
                        arrows: false
                    }
                }
            ]
        });

        $('.home-slider__btn--contact').on('click', function() {
            let target = $('.home-contact').offset().top - $('header').height();

            $('html, body').animate({
                scrollTop: target
            }, 300);
        });
    }

    // NOTE:Home about article
    if ( $('.home-about').length > 0 ) {
        let target = 'home-about';

        $('.'+ target +'__item').on('click', function() {
            let targetIndex = $(this).index();

            if ( $(this).hasClass(target + '__item--active') ) {
                $(this).removeClass(target + '__item--active');
                $('.'+ target +'__detailbox').removeClass(target + '__detailbox--active');
                $('.'+ target +'__detail').eq(targetIndex).hide(500);
            } else {
                let otherItem = $(this).siblings()
                let boxTop = $('.'+ target +'__detailbox').offset().top - $('.header-menu').height() - 20;

                $(this).addClass(target + '__item--active');

                if ( otherItem.hasClass(target + '__item--active') ) {
                    otherItem.removeClass(target + '__item--active');
                    $('.'+ target +'__detail').hide();
                    $('.'+ target +'__detail').eq(targetIndex).show();
                } else {
                    $('.'+ target +'__detailbox').addClass(target + '__detailbox--active');
                    $('.'+ target +'__detail').eq(targetIndex).show(500);
                }

                $('html, body').animate({
                    scrollTop: boxTop
                }, 300);
            }
        });

        $('.'+ target +'__detail-btn').on('click', function() {
            $(this).parents('.'+ target +'__detail').hide(500);
            $('.'+ target +'__item').removeClass(target + '__item--active');
            $('.'+ target +'__detailbox').removeClass(target + '__detailbox--active');
        });
    }

    // NOTE:Home qa article
    if ( $('.home-qa').length > 0 ) {
        let target = 'home-qa';

        $('.'+ target +'__item').on('click', function() {
            let targetIndex = $(this).index();

            if ( $(this).hasClass(target + '__item--active') ) {
                $(this).removeClass(target + '__item--active');
                $('.'+ target +'__detailbox').removeClass(target + '__detailbox--active');
                $('.'+ target +'__detail').eq(targetIndex).hide(500);
            } else {
                let otherItem = $(this).siblings()
                let boxTop = $('.'+ target +'__detailbox').offset().top - $('.header-menu').height() - 20;

                $(this).addClass(target + '__item--active');

                if ( otherItem.hasClass(target + '__item--active') ) {
                    otherItem.removeClass(target + '__item--active');
                    $('.'+ target +'__detail').hide();
                    $('.'+ target +'__detail').eq(targetIndex).show();
                } else {
                    $('.'+ target +'__detailbox').addClass(target + '__detailbox--active');
                    $('.'+ target +'__detail').eq(targetIndex).show(500);
                }

                $('html, body').animate({
                    scrollTop: boxTop
                }, 300);
            }
        });

        $('.'+ target +'__detail-btn').on('click', function() {
            $(this).parents('.'+ target +'__detail').hide(500);
            $('.'+ target +'__item').removeClass(target + '__item--active');
            $('.'+ target +'__detailbox').removeClass(target + '__detailbox--active');
        });
    }

    // NOTE:Scroll event
    if ( $('.scroll-effect').length > 0 ) {
        $(window).on('scroll resize', function() {
            $.each($('.scroll-effect'), function() {
                var trigger_point = $(window).scrollTop() + $(window).height() * 0.7;
                var ele_top = $(this).offset().top;

                if ( trigger_point >= ele_top ) {
                    $(this).addClass('scroll-effect--movein');
                }
            });
        });
    }

    // NOTE:Svg convert
    if ( $('.svg-convert').length > 0 ) {
        $('.svg-convert').svgConvert();
    }

    $(window)
        .trigger('scroll')
        .trigger('resize');
});

function show_success_msg(text) {
	$('body').append('<div class="home-contact__success">' + text + '</div>');
	setTimeout(function() { $('.home-contact__success').fadeOut(500); }, 1800);
	setTimeout(function() { $('.home-contact__success').remove(); }, 2300);
}

function show_error_msg(text) {
	$('body').append('<div class="home-contact__error">' + text + '</div>');
	setTimeout(function() { $('.home-contact__error').fadeOut(500); }, 1800);
	setTimeout(function() { $('.home-contact__error').remove(); }, 2300);
}