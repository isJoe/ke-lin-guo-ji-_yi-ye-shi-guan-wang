        <section class="home-care" style="background-image: url('<?php echo $block1_image['image_url']?>');" data-target>
            <div class="container">
                <div class="home-care__block">
                    <div class="common__title scroll-effect">
                        <p>您該知道的聽力保健資訊</p>
                    </div>

                    <div class="home-care__list">
                        <?php if (count($block1) > 0):
                            $i = 1;
                            foreach ( $block1 as $val ): ?>
                            <a href="<?php echo $val['link']?>" class="home-care__item scroll-effect <?php echo (($i % 2) == 0) ? 'scroll-effect--left' : 'scroll-effect--right' ?>" target="_blank">
                                <h3 class="home-care__title"><?php echo $val['title']?></h3>
                                <p class="home-care__text"><?php echo $val['intro']?></p>
                                <div class="home-care__btn">看更多</div>
                            </a>
                            <?php endforeach;
                        endif; ?>
                        <div class="home-care__img">
                            <img class="scroll-effect scroll-effect--bottom" src="<?php echo $block1_image['image2_url']?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>