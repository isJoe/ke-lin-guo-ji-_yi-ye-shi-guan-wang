        <section id="contact" class="home-contact" data-target>
            <div class="full-container">
                <div class="home-contact__block">
                    <div class="home-contact__form">
                        <div class="container">
                            <form class="home-contact__table scroll-effect scroll-effect--bottom">
                                <div class="home-contact__title">聯絡科林</div>

                                <div class="home-contact__item">
                                    <label class="home-contact__label">
                                        <div class="home-contact__field">您的姓名</div>
                                        <input type="text" name="contact_name" class="home-contact__input">
                                    </label>
                                </div>

                                <div class="home-contact__item">
                                    <label class="home-contact__label">
                                        <div class="home-contact__field">聯絡電話</div>
                                        <input type="tel" name="contact_phone" class="home-contact__input">
                                    </label>
                                </div>

                                <div class="home-contact__item">
                                    <div class="home-contact__field">居住地區</div>
                                    <div class="home-contact__radio">
                                        <label class="home-contact__radio-item">
                                            <input type="radio" name="contact_area" value="北區">北區
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="radio" name="contact_area" value="桃竹苗">桃竹苗
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="radio" name="contact_area" value="中區">中區
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="radio" name="contact_area" value="南區">南區
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="radio" name="contact_area" value="東區">東區
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="radio" name="contact_area" value="其他">其他
                                            <i></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="home-contact__item">
                                    <div class="home-contact__field">聯繫項目</div>
                                    <div class="home-contact__radio">
                                        <label class="home-contact__radio-item">
                                            <input type="checkbox" name="contact_item" value="預約聽檢">預約聽檢
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="checkbox" name="contact_item" value="聽力諮詢">聽力諮詢
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="checkbox" name="contact_item" value="了解助聽器">了解助聽器
                                            <i></i>
                                        </label>

                                        <label class="home-contact__radio-item">
                                            <input type="checkbox" name="contact_item" value="其他">其他
                                            <i></i>
                                        </label>
                                    </div>
                                </div>

                                <div class="home-contact__item">
                                    <div class="g-recaptcha" data-sitekey="6LdzK8YUAAAAAAdhnCqT_WFaYYgbhFVHGi-nYPV7"></div>
                                </div>

                                <div class="home-contact__btnbox">
                                    <a onclick="send_mail();" class="home-contact__btn">確認送出</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="home-contact__img scroll-effect scroll-effect--right"></div>
                </div>
            </div>
        </section>