        <section class="home-service" data-target>
            <div class="container">
                <div class="home-service__block">
                    <div class="common__title scroll-effect">
                        <p>我們的服務流程</p>
                        <span>專業完整的聽力評估，提供全方位聽力解決方案</span>
                    </div>

                    <div class="home-service__content scroll-effect scroll-effect--bottom">
                        <img class="home-service__img home-service__img--pc" src="images/home/home_service_pc-01.jpg" alt="">
                        <img class="home-service__img home-service__img--mb" src="images/home/home_service_mb-01.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>