        <section class="home-case" style="background-image: url('<?php echo $block2_image['image_url']?>');" data-target>
            <div class="container">
                <div class="home-case__block">
                    <div class="common__title scroll-effect">
                        <p>科林讚聲好友</p>
                    </div>

                    <div class="home-case__list">
                        <?php if (count($block2) > 0):
                            $i = 1;
                            foreach ( $block2 as $val ): ?>
                            <a href="<?php echo $val['link']?>" class="home-case__item scroll-effect <?php echo (($i % 2) == 0) ? 'scroll-effect--left' : 'scroll-effect--right' ?>" target="_blank">
                                <h3 class="home-case__title"><?php echo $val['title']?></h3>
                                <p class="home-case__text"><?php echo $val['intro']?></p>
                                <div class="home-case__btn">看更多</div>
                            </a>
                            <?php endforeach;
                        endif; ?>
                        <div class="home-case__img">
                            <img class="scroll-effect scroll-effect--bottom" src="<?php echo $block2_image['image2_url']?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>