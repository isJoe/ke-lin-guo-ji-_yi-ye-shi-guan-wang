<!doctype html>
<html>
<?php $this->load->view('common/head')?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<body>
    <?php $this->load->view('common/header')?>

    <div class="main">
        <?php if (count($banners) > 0): ?>
            <section class="home-slider">
                <div class="full-container">
                    <div class="home-slider__slick">
                        <?php foreach ( $banners as $key => $val ): ?>
                        <div class="home-slider__item home-slider__item-0<?php echo $key; ?>">
                            <div class="container">
                                <div class="home-slider__box">
                                    <h2 class="home-slider__title"><?php echo $val['title']?></h2>
                                    <p class="home-slider__subtitle"><?php echo $val['sub_title']?></p>
                                    <?php if ($val['link_text'] != '' && $val['link'] != '') :?>
                                    <div class="home-slider__btnbox">
                                        <a href="<?php echo $val['link']?>" class="home-slider__btn" target="_blank"><?php echo $val['link_text']?></a>
                                        <a class="home-slider__btn home-slider__btn--contact">立即聯絡</a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>

                    <?php foreach ( $banners as $key => $val ): ?>
                        <style>
                            .home-slider__item-0<?php echo $key; ?> {
                                background-image: url('<?php echo $val['image_url']?>');
                            }

                            @media (max-width: 991px) {
                                .home-slider__item-0<?php echo $key; ?> {
                                    background-image: url('<?php echo $val['image_mobile_url']?>');
                                }
                            }
                        </style>
                    <?php endforeach; ?>
                </div>
            </section>
        <?php endif; ?>

        <?php if ( $setting['home_block_0_status'] == '1' ): ?>
            <section class="home-banner">
                <div class="full-container">
                    <div class="home-banner__block scroll-effect scroll-effect--bottom">
                        <?php if ($setting['image'] != ''): ?>
                            <img class="home-banner__img home-banner__img--pc" src="<?php echo $setting['image_url']?>" alt="">
                        <?php endif; ?>
                        <?php if ($setting['image_mb'] != ''): ?>
                            <img class="home-banner__img home-banner__img--mb" src="<?php echo $setting['image_mb_url']?>" alt="">
                        <?php endif; ?>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <?php
            foreach ( $home_block_list as $template ) {
                $this->load->view($template);
            }
        ?>
    </div>

    <?php $this->load->view('common/footer')?>
    <?php $this->load->view('common/script')?>

    <script>
        is_lock = 'N';

        function send_mail () {
            var item_ary = new Array();

            if ( is_lock == 'Y' ) {
                show_error_msg('系統發送中，請稍後在試！');
            } else {
                is_lock = 'Y';

                $.each($('[name=contact_item]:checked'), function() {
                    item_ary.push($(this).val());
                });

                var item_str = item_ary.join('、');

                var post_data = {
                    'contact_name'  : $('[name=contact_name]').val(),
                    'contact_area'  : $('[name=contact_area]:checked').val(),
                    'contact_phone' : $('[name=contact_phone]').val(),
                    'contact_item'  : item_str,
                    'ga_code'       : $('#g-recaptcha-response').val()
                };

                $.post('<?php echo base_url('send_email') ?>',post_data,function(response){
                    if ( response['status']) {
                        $('[name=contact_name]').val('');
                        $('[name=contact_phone]').val('');
                        $('[name=contact_area], [name=contact_item]').prop('checked', false);
                        show_success_msg('感謝您的聯繫！我們將會盡速與您聯絡！');
                    } else {
                        show_error_msg(response['error']);
                    }
                    grecaptcha.reset();
                    is_lock = 'N';
                });
            }
        }
    </script>
</body>
</html>