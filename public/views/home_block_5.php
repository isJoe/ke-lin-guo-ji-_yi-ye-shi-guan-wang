        <?php if (count($question) > 0): ?>
            <section class="home-qa" data-target>
                <div class="container">
                    <div class="home-qa__block">
                        <div class="common__title scroll-effect">
                            <p>常見問題Q&A</p>
                        </div>
                        <?php $i = 1;?>
                        <div class="home-qa__detailbox">
                            <?php foreach ( $question as $val ): ?>
                                <article class="home-qa__detail">
                                    <?php if ($val['image2'] != ''): ?>
                                        <div class="home-qa__detail-img">
                                            <img src="<?php echo $val['image2_url']?>" alt="">
                                        </div>
                                    <?php endif;?>

                                    <div class="home-qa__detail-box">
                                        <div class="home-qa__detail-title">
                                            <h3><?php echo $val['title']?></h3>
                                        </div>

                                        <div class="home-qa__detail-content cke_editable">
                                            <?php echo html_entity_decode($val['description']) ?>
                                        </div>

                                        <div class="home-qa__detail-btnbox">
                                            <a class="home-qa__detail-btn"></a>
                                        </div>
                                    </div>
                                </article>
                            <?php endforeach; ?>
                        </div>
                        <?php $i = 1;?>
                        <div class="home-qa__list">
                            <?php foreach ( $question as $val ): ?>
                                <a class="home-qa__item scroll-effect <?php echo (($i % 2) == 0) ? 'scroll-effect--left' : 'scroll-effect--right' ?>">
                                    <div class="home-qa__img" style="background-image: url('<?php echo $val['image_url']?>');"></div>
                                    <div class="home-qa__content">
                                        <h3 class="home-qa__title"><?php echo $val['title']?></h3>
                                        <p class="home-qa__text"><?php echo $val['intro']?></p>
                                    </div>
                                </a>
                                <?php $i++;?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>