        <?php if (count($news) > 0): ?>
            <section class="home-about" data-target>
                <div class="container">
                    <div class="home-about__block">
                        <div class="common__title scroll-effect">
                            <p>關於科林</p>
                        </div>
                        <?php $i = 1;?>
                        <div class="home-about__detailbox">
                            <?php foreach ( $news as $val ): ?>
                                <article class="home-about__detail">
                                    <?php if ($val['image2'] != ''): ?>
                                        <div class="home-about__detail-img">
                                            <img src="<?php echo $val['image2_url']?>" alt="">
                                        </div>
                                    <?php endif; ?>

                                    <div class="home-about__detail-box">
                                        <div class="home-about__detail-title">
                                            <h3><?php echo $val['title']?></h3>
                                        </div>

                                        <div class="home-about__detail-content cke_editable">
                                            <?php echo html_entity_decode($val['description']) ?>
                                        </div>

                                        <div class="home-about__detail-btnbox">
                                            <a class="home-about__detail-btn"></a>
                                        </div>
                                    </div>
                                </article>
                            <?php endforeach; ?>
                        </div>
                        <?php $i = 1;?>
                        <div class="home-about__list">
                            <?php foreach ( $news as $val ): ?>
                                <a class="home-about__item scroll-effect <?php echo (($i % 2) == 0) ? 'scroll-effect--left' : 'scroll-effect--right' ?>">
                                    <div class="home-about__img" style="background-image: url('<?php echo $val['image_url']?>');"></div>
                                    <div class="home-about__content">
                                        <h3 class="home-about__title"><?php echo $val['title']?></h3>
                                        <p class="home-about__text"><?php echo $val['intro']?></p>
                                    </div>
                                    <?php $i++;?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>