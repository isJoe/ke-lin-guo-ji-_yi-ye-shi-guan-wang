<head>
    <?php if (isset($setting['head_after']) && $setting['head_after'] != '') {
        echo html_entity_decode($setting['head_after']);
    }?>
    <!-- NOTE:Meta common -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta name="format-detection" content="telephone=no">

    <!-- NOTE:Meta path -->
    <base href="<?php echo base_url('lazyweb/web/') ?>">

    <!-- NOTE:Meta favicon -->
    <link href="images/favicon.png" rel="shortcut icon" type="image/png">

    <!-- NOTE:Stylesheet -->
    <link href="css/slick.css" rel="stylesheet">
    <link href="css/clinico.css<?php echo '?' . time(); ?>" rel="stylesheet">
    <link href="css/ck_editor.css" rel="stylesheet">

    <!-- NOTE:Jquery -->
    <script src="js/jquery.js"></script>

    <!-- NOTE:Google font -->
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:400,500,700&display=swap&subset=chinese-traditional" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">

    <!-- NOTE:Meta seo -->
    <meta name="author" content="" />
    <meta name="copyright" content="" />

    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <meta itemprop="name" content="<?php echo $meta_title ?>" />
    <meta itemprop="image" content="/lazyweb/web/images/share.jpg" />
    <meta itemprop="description" content="" />

    <meta property="og:title" content="<?php echo $meta_title ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" />
    <meta property="og:image" content="/lazyweb/web/images/share.jpg" />
    <meta property="og:description" content="" />

    <title><?php echo $meta_title ?></title>
    <?php if (isset($setting['head_before']) && $setting['head_before'] != '') {
        echo html_entity_decode($setting['head_before']);
    }?>
</head>
