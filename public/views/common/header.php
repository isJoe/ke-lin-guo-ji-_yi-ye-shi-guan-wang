    <?php if (isset($setting['body_before']) && $setting['body_before'] != '') {
        echo html_entity_decode($setting['body_before']);
    }?>
    <div class="fixed__social">
        <a id="go2contact" class="fixed__social-btn fixed__social-btn--contact" target="_blank"></a>
        <a href="tel:<?php echo LINK_TEL ?>" class="fixed__social-btn fixed__social-btn--tel" target="_blank"></a>
        <a href="<?php echo LINK_FB ?>" class="fixed__social-btn fixed__social-btn--fb" target="_blank"></a>
        <a href="<?php echo LINK_LINE ?>" class="fixed__social-btn fixed__social-btn--line" target="_blank"></a>
    </div>

    <header class="header">
        <h1>科林助聽器</h1>

        <div class="header-menu">
            <div class="container">
                <div class="header-menu__block">
                    <a href="<?php echo base_url()?>" class="header-menu__logo">
                        <img src="images/logo.svg">
                    </a>

                    <div class="header-menu__nav">
                        <div class="header-menu__navbox">
                            <?php foreach ( $menu_list as $menu ) { ?>
                            <a class="header-menu__btn"><?php echo $menu ?></a>
                            <?php } ?>
                        </div>
                    </div>

                    <a class="header-menu__burger">
                        <div></div>
                        <div></div>
                        <div></div>
                    </a>

                    <a class="header-menu__close">
                        <div></div>
                        <div></div>
                    </a>
                </div>
            </div>
        </div>
    </header>