    <footer class="footer">
        <div class="footer-main">
            <div class="container">
                <div class="footer-main__block">
                    <div class="footer-main__logo">
                        <img src="images/logo.svg" alt="">
                    </div>
                    <div class="footer-main__text">
                        <p>唯一通過SGS ISO 9001、ISO 13485、Qualicert 國際服務品質驗證等國際認證之助聽器公司</p>
                        <p>一家選配 全臺服務</p>
                    </div>

                    <div class="footer-main__social">
                        <a href="tel:<?php echo LINK_TEL ?>" class="footer-main__social-btn footer-main__social-btn--tel" target="_blank"></a>
                        <a href="<?php echo LINK_FB ?>" class="footer-main__social-btn footer-main__social-btn--fb" target="_blank"></a>
                        <a href="<?php echo LINK_LINE ?>" class="footer-main__social-btn footer-main__social-btn--line" target="_blank"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-copyright">
            <div class="container">
                <div class="footer-copyright__text">
                    Clinico © All right reserved.
                </div>
            </div>
        </div>
    </footer>

    <a class="gototop"></a>