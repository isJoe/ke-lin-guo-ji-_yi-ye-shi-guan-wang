<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Site extends MY_Controller
{
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Home_model'));
        $this->data['page_active'] = 'home';
    }

    public function index()
    {
        # 輪播
        $banners = $this->Home_model->get_banners();
        $this->data['banners'] = array();
        foreach ($banners as $key => $row) {
            $this->data['banners'][$key] = $row;
            $this->data['banners'][$key]['image_url'] = $this->get_image_url($row['image']);
            $this->data['banners'][$key]['image_mobile_url'] = $this->get_image_url($row['image_mobile']);
        }
        # 系統設定
        $setting = $this->Home_model->get_setting();
        if ($setting['image'] != '') {
            $setting['image_url'] = $this->get_image_url($setting['image']);
        }
        if ($setting['image_mb'] != '') {
            $setting['image_mb_url'] = $this->get_image_url($setting['image_mb']);
        }
        $this->data['setting'] = $setting;

        # 區塊順序設定
        $menu_list = array();
        $seq_list = array();
        for ( $i = 1 ; $i <= 6 ; $i++ ) {
            if ( $i == $setting['home_block_1'] && $setting['home_block_1_status'] == '1' ) {
                $seq_list[$i] = 'home_block_1';
                $menu_list[$i] = '關於科林';
            }
            if ( $i == $setting['home_block_2'] && $setting['home_block_2_status'] == '1' ) {
                $seq_list[$i] = 'home_block_2';
                $menu_list[$i] = '聽力保健';
            }
            if ( $i == $setting['home_block_3'] && $setting['home_block_3_status'] == '1' ) {
                $seq_list[$i] = 'home_block_3';
                $menu_list[$i] = '服務流程';
            }
            if ( $i == $setting['home_block_4'] && $setting['home_block_4_status'] == '1' ) {
                $seq_list[$i] = 'home_block_4';
                $menu_list[$i] = '讚聲好友';
            }
            if ( $i == $setting['home_block_5'] && $setting['home_block_5_status'] == '1' ) {
                $seq_list[$i] = 'home_block_5';
                $menu_list[$i] = '常見QA';
            }
            if ( $i == $setting['home_block_6'] && $setting['home_block_6_status'] == '1' ) {
                $seq_list[$i] = 'home_block_6';
                $menu_list[$i] = '聯絡科林';
            }
        }
        $this->data['home_block_list'] = $seq_list;
        $this->data['menu_list'] = $menu_list;

        # 關於科林
        $news = $this->Home_model->get_news();
        $this->data['news'] = array();
        foreach ($news as $key => $row) {
            $this->data['news'][$key] = $row;
            $this->data['news'][$key]['image_url'] = $this->get_image_url($row['image']);
            $this->data['news'][$key]['image2_url'] = $this->get_image_url($row['image2']);
        }

        # 聽力保健
        $block1 = $this->Home_model->get_block(1);
        $this->data['block1'] = $block1;
        $block1_image = $this->Home_model->get_block_image(1);
        $this->data['block1_image'] = array();
        if (count($block1_image) > 0) {
            $block1_image['image_url'] = $this->get_image_url($block1_image['image']);
            $block1_image['image2_url'] = $this->get_image_url($block1_image['image2']);
            $this->data['block1_image'] = $block1_image;
        }

        # 讚聲好友
        $block2 = $this->Home_model->get_block(2);
        $this->data['block2'] = $block2;
        $block2_image = $this->Home_model->get_block_image(2);
        $this->data['block2_image'] = array();
        if (count($block2_image) > 0) {
            $block2_image['image_url'] = $this->get_image_url($block2_image['image']);
            $block2_image['image2_url'] = $this->get_image_url($block2_image['image2']);
            $this->data['block2_image'] = $block2_image;
        }

        # 常見問題
        $question = $this->Home_model->get_question();
        $this->data['question'] = array();
        foreach ($question as $key => $row) {
            $this->data['question'][$key] = $row;
            $this->data['question'][$key]['image_url'] = $this->get_image_url($row['image']);
            $this->data['question'][$key]['image2_url'] = $this->get_image_url($row['image2']);
        }

        # seo 設定
        $this->data['meta_title'] = '科林助聽器';
        $this->data['meta_keyword'] = '';
        $this->data['meta_description'] = '';
        $this->data['meta_image'] = $this->get_default_website_image();

        $this->auto_view('home', $this->data);
    }

    public function send_email()
    {
        $return['status'] = false;
        $return['error'] = '';

        $data = $this->input->post();

        $name    = isset($data['contact_name']) ? trim($data['contact_name']) : '';
        $phone   = isset($data['contact_phone']) ? trim($data['contact_phone']) : '';
        $area    = isset($data['contact_area']) ? trim($data['contact_area']) : '';
        $item    = isset($data['contact_item']) ? trim($data['contact_item']) : '';

        if ($item == '') {
            $return['error'] = '請選擇 聯繫項目';
        }
        if ($area == '') {
            $return['error'] = '請選擇 居住地區';
        }
        if ($phone == '') {
            $return['error'] = '請輸入 電話';
        }
        if ($name == '') {
            $return['error'] = '請輸入 姓名';
        }

        if ($return['error'] == '') {
            //GOOGLE 我不是機器人驗證
            $ch = curl_init();
            $secretKey = '6LdzK8YUAAAAALqw_5C8Tc2cKORM59srm1UYg4jy';
            $captcha = $this->input->post('ga_code');
            curl_setopt_array($ch, [
                CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => [
                    'secret' => $secretKey,
                    'response' => $captcha,
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                ],
                CURLOPT_RETURNTRANSFER => true
            ]);

            $output = curl_exec($ch);
            curl_close($ch);

            $json = json_decode($output);

            if (isset($json->success) && $json->success) {
                $txt = '<table style="width: 500px;border: 1px solid #E3E3E3;padding: 1px;">
                  <thead>
                    <tr>
                      <th colspan="2"  style="border-radius: .25rem .25rem 0 0;cursor: auto;background: #f0f0f0;text-align: left;color: rgba(0,0,0,.8);
                padding: .7em .8em;font-style: none;font-weight: 700;text-transform: none;border-bottom: 1px solid #d4d4d5;">客戶詢問信</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td style="height: 40px;border-bottom: 1px solid #ccc;color:#777676;padding: 0px 12px;">姓名</td>
                      <td style="border-bottom: 1px solid #ccc">' . $name . '</td>
                    </tr>
                    <tr>
                      <td style="height: 40px;border-bottom: 1px solid #ccc;color:#777676;padding: 0px 12px;">聯絡電話</td>
                      <td style="border-bottom: 1px solid #ccc">' . $phone . '</td>
                    </tr>
                    <tr>
                      <td style="height: 40px;border-bottom: 1px solid #ccc;color:#777676;padding: 0px 12px;">居住地區</td>
                      <td style="border-bottom: 1px solid #ccc">' . $area . '</td>
                    </tr>
                    <tr>
                      <td style="height: 40px;border-bottom: 1px solid #ccc;color:#777676;padding: 0px 12px;">聯繫項目</td>
                      <td style="border-bottom: 1px solid #ccc">' . $item . '</td>
                    </tr>
                  </tbody>
                </table>';

                $post_data = array(
                    'subject' => "客戶諮詢 - 科林助聽器",
                    'txt' => $txt,
                    'send' => MAIL_add,
                );

                $send_status = $this->send_mail_by_lazyweb($post_data);

                if ( $send_status  == '1') {
                    $return['status'] = true;
                } else {
                    $return['error'] = '系統忙碌中! 建議稍後再嘗試!';
                }
            } else {
                $return['error'] = '驗證失敗，請重新操作！';
            }
        }

        $this->output->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($return));
    }

    private function send_mail_by_lazyweb($post_data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.lazyweb.net.tw/send_mail.php");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
        $send = curl_exec($ch);
        curl_close($ch);

        return $send;
    }
}
