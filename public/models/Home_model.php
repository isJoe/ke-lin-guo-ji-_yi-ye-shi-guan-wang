<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_banners ()
    {
        $home_banners = $this->db->dbprefix('home_banners');

        $sql = "
            SELECT *
            FROM {$home_banners}
            WHERE status = '1'
            ORDER BY sort_order ASC
        ";

        $query = $this->db->query($sql);
        return  $query->result_array();
    }

    public function get_setting ()
    {
        $system_setting = $this->db->dbprefix('system_setting');

        $sql = "
            SELECT *
            FROM {$system_setting}
        ";

        $query = $this->db->query($sql);
        return  $query->row_array();
    }

    public function get_news ()
    {
        $news = $this->db->dbprefix('news');

        $sql = "
            SELECT *
            FROM {$news}
            WHERE status = '1'
            ORDER BY sort_order ASC
        ";

        $query = $this->db->query($sql);
        return  $query->result_array();
    }

    public function get_block ($block_id)
    {
        $block = $this->db->dbprefix('block');
        $block_id = $this->db->escape($block_id);

        $sql = "
            SELECT *
            FROM {$block}
            WHERE block_id = {$block_id}
            ORDER BY sort_order ASC
        ";

        $query = $this->db->query($sql);
        return  $query->result_array();
    }

    public function get_block_image ($block_id)
    {
        $block_image = $this->db->dbprefix('block_image');
        $block_id = $this->db->escape($block_id);

        $sql = "
            SELECT *
            FROM {$block_image}
            WHERE block_id = {$block_id}
        ";

        $query = $this->db->query($sql);
        return  $query->row_array();
    }

    public function get_question ()
    {
        $question = $this->db->dbprefix('question');

        $sql = "
            SELECT *
            FROM {$question}
            WHERE status = 1
            ORDER BY sort_order ASC
        ";

        $query = $this->db->query($sql);
        return  $query->result_array();
    }
}
