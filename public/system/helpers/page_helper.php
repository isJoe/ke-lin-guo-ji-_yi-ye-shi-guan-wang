<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * CodeIgniter Text Helpers
 *
 * @package        LAZYWEB
 * @subpackage    Helpers
 * @category    Helpers
 * @author        Joe
 */
// ------------------------------------------------------------------------
if (!function_exists('page_logic_factory')) {
    /**
     * 頁數邏輯製造器
     *
     * @param    int | 傳入當前的頁數 | 預設 1
     * @param    int | 目前有多少資料 | 預設 0
     * @param    int | 一頁包含多少資料 | 預設 10
     * @param    int | 該頁數一次顯示幾筆 | 預設 5 | 限制基數單位
     * @return    array
     */
    function page_logic_factory( $page = 1, $data_total = 0, $limit = 10, $page_show_limit = 5 )
    {
        $pageRange = ceil($data_total / $limit) ; 

        $page_model['page'] = $page ;
        $page_model['viewRangFirst'] = '' ;
        $page_model['viewRange'] = array();
        $page_model['viewRangeController'] = array();
        $page_model['viewRangLast'] = '' ;

        if ( $pageRange <= 0 ) {
            $page_model['viewRange'][1] = '1';
        } else {
            for ( $p = 1 ; $p <= $pageRange ; $p++ ) {                
                $page_model['viewRange'][$p] = $p;
            }
            $page_model['viewRangFirst'] = $page_model['viewRange'][1] ;
            if ( $page_model['viewRangFirst'] == $page ) {
                $page_model['viewRangFirst'] = '';
            }
            $page_model['viewRangLast'] = $page_model['viewRange'][count($page_model['viewRange'])] ;
            if ( $page_model['viewRangLast'] == $page ) {
                $page_model['viewRangLast'] = '';
            }

            if ( $page_show_limit % 2 != '0' ) {
                $page_model['viewRangeController'][$page] = $page ; 
                $left_num = floor( $page_show_limit / 2 ); // 左邊預設數量
                $right_num = floor( $page_show_limit / 2 ); // 右邊預設數量

                //echo 'left_num :' . $left_num . '<br/>';
                //echo 'right_num :' . $right_num . '<br/>';

                $right_counter = $right_num ; 
                # 抓右邊的頁數
                for ( $r = $page ; $r <= count($page_model['viewRange']) ; $r++ ) {
                    if ( $right_counter > 0 ) {
                        if ( isset($page_model['viewRange'][$r]) && !isset($page_model['viewRangeController'][$r]) ) {
                            $page_model['viewRangeController'][$r] = $r;
                            $right_counter = $right_counter - 1 ;
                        }
                    }
                }                 

                //echo 'right_counter :' . $right_counter . '<br/>';

                $left_counter = $left_num + $right_counter ; ## 假設右邊數量不夠,額度給左邊的使用
                # 抓左邊的頁數
                for ( $l = $page ; $l > 0 ; $l-- ) {
                    if ( $left_counter > 0 ) {
                        if ( isset($page_model['viewRange'][$l]) && !isset($page_model['viewRangeController'][$l]) ) {
                            $page_model['viewRangeController'][$l] = $l;
                            $left_counter = $left_counter - 1 ;
                        }
                    }
                }    
                //echo 'left_counter :' . $left_counter . '<br/>';

                ksort($page_model['viewRangeController']); // 來個排序            

                # 如果左邊的頁數抓不夠,再跟右邊多要一點頁數
                if ( $left_counter > 0 ) {
                    $left_last_key = $page_model['viewRangeController'][count($page_model['viewRangeController'])];
                    
                    $last_right_counter = $left_counter ;                    
                    // echo 'last_right_counter :' . $last_right_counter . '<br/>';
                    for ( $i = $left_last_key ; $i <= count($page_model['viewRange']) ; $i++ ) {
                        if ( $last_right_counter > 0 ) {
                            if ( isset($page_model['viewRange'][$i]) && !isset($page_model['viewRangeController'][$i]) ) {
                                $page_model['viewRangeController'][$i] = $i ; 
                                $last_right_counter = $last_right_counter - 1 ;
                            }
                        }
                    }

                    ksort($page_model['viewRangeController']); // 通通補完了,再重新排序
                }
                
            }
        }
        
        if ( !isset($page_model['viewRange'][$page]) ) { // 如果頁數不合格,就取消結構
            $page_model['viewRangFirst'] = '' ;
            $page_model['viewRange'] = array();
            $page_model['viewRangeController'] = array();
            $page_model['viewRangLast'] = '' ;            
        } else if ( count($page_model['viewRange']) == '1' ) { // 如果只有一頁,就取消結構
            $page_model['viewRangFirst'] = '' ;
            $page_model['viewRange'] = array();
            $page_model['viewRangeController'] = array();
            $page_model['viewRangLast'] = '' ;    
        }
        return $page_model ; 
    }
}
