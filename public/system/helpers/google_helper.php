<?php
function get_google_address_geometry($googleQuery='',$key='',$isHttps='Y') {
	
	$data = array ();
	$data['success'] = 0 ;
	if ( $googleQuery == '' ) {
		$googleQuery = '101' ;
	}
	
	if ( $key == '' ) {
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($googleQuery).'&sensor=false&language=zh-TW';
	} else {
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($googleQuery).'&sensor=false&language=zh-TW&key=' .$key  ;
	}
	$ch  = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if ( $isHttps == 'Y' ) {
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
	}
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 5); // 逾時設置
	
	$result = json_decode(curl_exec($ch),TRUE); // 將回傳的資料 轉換成 JSON	

	//return $result ;
	curl_close($ch);	
	
	if ( $result["status"] == "OK" ) {
		$mapsData = isset($result["results"][0]["geometry"]) ? $result["results"][0]["geometry"] : array() ;
		
		if ( isset($result["results"][0]["geometry"]['location']['lat']) && isset($result["results"][0]["geometry"]['location']['lng'])  ) {
			$data['success'] = 1 ;
			$data['location'] = $result["results"][0]["geometry"]['location'] ;
		}
	} 		
	return $data ; 	
}
