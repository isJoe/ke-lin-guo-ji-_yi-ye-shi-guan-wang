<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 檢查用戶登入狀況 Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @author		LazywWeb Joe.
 */
class CI_User {
	/*
	public $ci;
	function __construct() {
		$CI =& get_instance();
		$CI->load->model('Dashboard_Model');
		$this->ci = $CI;
	}
	*/
	/**
	 * 取得 用戶 ID
	 */	
	public function id() {
		return isset( $_SESSION['customer'] ) ? $_SESSION['customer']['customer_id'] : '' ;
	}
	
	/**
	 * 取得 用戶 角色
	 */	
	public function identity() {
		$rule = '';
		if ( self::check_user_login_status() ) {
			$rule = $_SESSION['customer_rule']['name'];
		}
		return $rule ; 
	}

	/**
	 * 取得 用戶 大頭貼
	 */	
	public function image() {
		$image = '';
		if ( self::check_user_login_status() ) {
			$image = $_SESSION['customer']['image'];
		}
		return $image ; 
	}

	
	/**
	 * 取得 用戶 角色 投資權限
	 */	
	public function permission() {
		return isset( $_SESSION['customer_rule'] ) ? $_SESSION['customer_rule'] : array() ;
	}
	
	/**
	 * 確認用戶登入的狀態
	 */	
	public function check_user_login_status () {
		$is_login = False ;
		if ( isset( $_SESSION['customer']['customer_id'] ) && $_SESSION['customer']['customer_id'] != '' ) {
			$is_login = TRUE ;
		}
		return $is_login ;
	}
	
	/**
	 * 確認用戶是用哪種方式註冊的	 
	 */		
	public function get_user_register_method () {
		$register_method = '' ;		
		if ( self::check_user_login_status() ) {
			$register_method = $_SESSION['customer']['register_method'] ; 
		}	
		return $register_method ;
	}
	
	/**
	 * 得到用戶暱稱 
	 */	
	public function get_user_name () {
		$name = '' ;		
		if ( self::check_user_login_status() ) {
			$name = $_SESSION['customer']['name'] ; 
		}	
		return $name ;
	}	 


	/**
	 * 更新用戶資訊
	 */	
	public function update_user_info ($customerinfo=array()) {
		$_SESSION['customer']['name'] = $customerinfo['name'];
		$_SESSION['customer']['image'] = $customerinfo['image'];
		$_SESSION['customer']['telephone'] = $customerinfo['telephone'];
		$_SESSION['customer']['firstname'] = $customerinfo['firstname'];
		$_SESSION['customer']['lastname'] = $customerinfo['lastname'];
	}


	/**
	 * 登出目前的帳號 
	 */	
	public function logout () {		
		if ( isset($_SESSION['customer']) ) {
			unset($_SESSION['customer']);
		}		
		if ( isset($_SESSION['customer_rule']) ) {
			unset($_SESSION['customer_rule']);
		}				
	}	
	
	/**
	 * 得到 密碼加密資訊 
	 */	
	public function get_password_encryption_info ( $password = '' ) {		
		$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		$password_md5 = sha1($salt . sha1($salt . sha1($password)));		
		return array( 'salt' => $salt , 'password' => $password_md5  ) ;
	}
	
	/**
	 *  驗證 一般登入的用戶密碼是否正常 
	 */		
	public function validate_user_password_encryption ( $salt='' , $password = '' ) {		
		return sha1( $salt . sha1($salt . sha1($password)) );
	}	 
	
}