<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'Site';

$route['send_email'] = 'Site/send_email';

$route['404_override'] = 'Rewrite';
$route['translate_uri_dashes'] = true;
