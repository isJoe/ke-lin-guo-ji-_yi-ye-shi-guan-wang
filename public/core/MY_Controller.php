<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    private $header_data = array();
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('utf8', 'url', 'cookie'));
        $this->load->library(array());

        if ($this->input->server('REQUEST_SCHEME') != 'https' && ENVIRONMENT == 'production') {
            redirect('https://' . $this->input->server('SERVER_NAME') . $this->input->server('REQUEST_URI'));
        }
    }

    public function get_image_url ( $image_path = '' ) {
        $image_url = '';
        $image_path = trim($image_path);
        if ( FTP_MODE == '2' ) {
            if ( $image_path == '' ) {
                $image_url = SYNC_HTTPS . 'no_image.jpg' ;
            } else {
                $image_url = SYNC_HTTPS . 'image/' . $image_path ;
            }
        } else {
            if ( $image_path != '' && file_exists(DIR_IMAGE . $image_path) ) {
                $image_url = base_url('image/'.$image_path);
            } else {
                $image_url = base_url('image/no_image.jpg');
            }
        }
        return $image_url ;
    }

    public function get_default_website_image ()
    {
        return base_url('lazyweb/web/images/share.jpg');
    }

    // 封裝後的 view output
    public function auto_view($view, $data = array(), $option = array())
    {
        if ( isset($_GET['show_data']) && ENVIRONMENT != 'production' ) {

            $php_parameter['$_GET'] = $_GET ;
            $php_parameter['$_POST'] = $_POST ;

            $ci_parameter['$_GET'] = $this->input->get();
            $ci_parameter['$_POST'] = $this->input->post();

            $print['php_parameter'] = $php_parameter ;
            $print['ci_parameter'] = $ci_parameter ;
            $print['view輸出變數打印'] = $data ;

            echo '<pre>'; print_r($print);

        } else {
            $this->load->view($view, $data);
        }
    }
}
